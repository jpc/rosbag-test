(self.webpackChunk=self.webpackChunk||[]).push([[323],{39859:(pa,Ft,y)=>{"use strict";y.r(Ft),y.d(Ft,{default:()=>ua});var C=y(52322),g=y(2784),Ps=y(28316),As=y(92168),Rs=y(18906),Ns=y(36843),Ut=y(78951),Os=y(62515),Ge=y(23503),at=y(41271),zs=y(44668),Gs=y(65510),Fs=y(43860);const Bt=(0,g.createContext)(null);function Fe(){return(0,g.useContext)(Bt)??void 0}function xe(s,e){const t=Fe();(0,g.useEffect)(()=>(t?.addListener(s,e),()=>void t?.removeListener(s,e)),[e,s,t])}function Us(){const[s,e]=(0,g.useState)(null),t=Fe();return(0,g.useEffect)(()=>{if(!t||!s)return;const i=Bs(t,s);return()=>void s.removeChild(i.domElement)},[s,t]),(0,C.jsx)("div",{ref:e})}function Bs(s,e){const t=new Fs.XS({autoPlace:!1,width:300});e.appendChild(t.domElement);const i=t.addFolder("Renderer");return i.add(s.gl,"toneMappingExposure",0,2,.01),i.add(s.gl,"physicallyCorrectLights",!1),t}var r=y(96995);const jt=44,_e=64,Ht=4,js=16,Ie=new r.Pa4;function ma(s){return _jsx("div",{ref:s.innerRef,style:{display:s.visible?"inherit":"none",position:"absolute",width:_e,height:_e,pointerEvents:"none"}})}function $t(s,e,t,i){Ie.copy(e),Ie.project(i);const n=(Ie.x*.5+.5)*t.width,a=(Ie.y*-.5+.5)*t.height,o=Ie.distanceTo(i.position),c=1-r.M8C.clamp((o-Ht)/(js-Ht),0,1),h=r.M8C.lerp(jt,_e,c),d=r.M8C.lerp(jt/_e,1,c),f=-(_e/2)+n%1,p=-h+a%1;s.left=`${Math.floor(n)}px`,s.top=`${Math.floor(a)}px`,s.transform=`translate(${f}px, ${p}px) scale(${d})`}var Hs=y(10161),Vt=y.n(Hs);class ae extends r.Tme{constructor(){super();const e=new r.jyz({transparent:!0,depthTest:!1,depthWrite:!1,uniforms:{},vertexShader:`
        void main() {
          gl_Position = vec4(position.xy, 0.0, 1.0);
        }`,fragmentShader:`
        void main() {
          gl_FragColor = vec4(0.0, 0.0, 0.0, 0.75);
        }
      `}),t=new r.Kj0(ae.Geometry(),e);this.add(t)}static Geometry(){return ae.geometry||(ae.geometry=new r._12(2,2,1,1),ae.geometry.computeBoundingSphere()),ae.geometry}}const $s=1,Vs=new r.FM8;class Ws extends Vt(){constructor(e){super();this.cursorCoords=new r.FM8,this.onResize=i=>{if(this.canvas.parentElement){const n=Ys(this.canvas.parentElement);if(isNaN(n.width)||isNaN(n.height))return;(n.width!==this.canvasSize.width||n.height!==this.canvasSize.height)&&(this.canvasSize.width=n.width,this.canvasSize.height=n.height,this.emit("resize",this.canvasSize))}},this.onMouseDown=i=>{this.startClientPos=new r.FM8(i.offsetX,i.offsetY),this.emit("mousedown",this.cursorCoords,i)},this.onMouseMove=i=>{this.updateCursorCoords(i),this.emit("mousemove",this.cursorCoords,i)},this.onClick=i=>{if(!this.startClientPos)return;const n=this.startClientPos.distanceTo(Vs.set(i.offsetX,i.offsetY));this.startClientPos=void 0,!(n>$s)&&(this.updateCursorCoords(i),this.emit("click",this.cursorCoords,i))},this.onTouchStart=i=>{const n=i.touches[0];n&&(this.startClientPos=new r.FM8(n.clientX,n.clientY)),i.preventDefault()},this.onTouchEnd=i=>{i.preventDefault()},this.onTouchMove=i=>{i.preventDefault()},this.onTouchCancel=i=>{i.preventDefault()};const t=e.parentElement;if(!t)throw new Error("<canvas> must be parented to a DOM element");this.canvas=e,this.canvasSize=new r.FM8(e.width,e.height),this.resizeObserver=new ResizeObserver(this.onResize),this.resizeObserver.observe(t),e.addEventListener("mousedown",this.onMouseDown),e.addEventListener("mousemove",this.onMouseMove),e.addEventListener("click",this.onClick),e.addEventListener("touchstart",this.onTouchStart,{passive:!1}),e.addEventListener("touchend",this.onTouchEnd,{passive:!1}),e.addEventListener("touchmove",this.onTouchMove,{passive:!1}),e.addEventListener("touchcancel",this.onTouchCancel,{passive:!1})}dispose(){const e=this.canvas;this.removeAllListeners(),this.resizeObserver.disconnect(),e.removeEventListener("mousedown",this.onMouseDown),e.removeEventListener("mousemove",this.onMouseMove),e.removeEventListener("click",this.onClick),e.removeEventListener("touchstart",this.onTouchStart),e.removeEventListener("touchend",this.onTouchEnd),e.removeEventListener("touchmove",this.onTouchMove),e.removeEventListener("touchcancel",this.onTouchCancel)}updateCursorCoords(e){this.cursorCoords.x=e.offsetX,this.cursorCoords.y=e.offsetY}}function Ys(s){const e=getComputedStyle(s),t=parseFloat(e.paddingLeft)+parseFloat(e.paddingRight),i=parseFloat(e.paddingTop)+parseFloat(e.paddingBottom),n=parseFloat(e.borderLeftWidth)+parseFloat(e.borderRightWidth),a=parseFloat(e.borderTopWidth)+parseFloat(e.borderBottomWidth),o=s.clientWidth-t-n,c=s.clientHeight-i-a;return{width:o,height:c}}var Zs="../../packages/studio-base/src/panels/ThreeDeeRender/LayerErrors.ts";const Xs=Ge.Z.getLogger(Zs);class Ks{constructor(){this.errors=new Map}addToLayer(e,t,i){let n=this.errors.get(e);n||(n=new Map,this.errors.set(e,n)),n.set(t,i),Xs.warn(`[${e}] ${t}: ${i}`)}addToTopic(e,t,i){this.addToLayer(`t:${e}`,t,i)}removeFromLayer(e,t){const i=this.errors.get(e);i&&i.delete(t)}removeFromTopic(e,t){this.removeFromLayer(`t:${e}`,t)}clearLayer(e){this.errors.delete(e)}clearTopic(e){this.clearLayer(`t:${e}`)}clear(){this.errors.clear()}}r.rBU.line={worldUnits:{value:1},linewidth:{value:1},resolution:{value:new r.FM8(1,1)},dashOffset:{value:0},dashScale:{value:1},dashSize:{value:1},gapSize:{value:1}},r.Vj0.line={uniforms:r.rDY.merge([r.rBU.common,r.rBU.fog,r.rBU.line]),vertexShader:`
    #include <common>
    #include <color_pars_vertex>
    #include <fog_pars_vertex>
    #include <logdepthbuf_pars_vertex>
    #include <clipping_planes_pars_vertex>

    uniform float linewidth;
    uniform vec2 resolution;

    attribute vec3 instanceStart;
    attribute vec3 instanceEnd;

    attribute vec4 instanceColorStart;
    attribute vec4 instanceColorEnd;

    #ifdef WORLD_UNITS

      varying vec4 worldPos;
      varying vec3 worldStart;
      varying vec3 worldEnd;

      #ifdef USE_DASH

        varying vec2 vUv;

      #endif

    #else

      varying vec2 vUv;

    #endif

    #ifdef USE_DASH

      uniform float dashScale;
      attribute float instanceDistanceStart;
      attribute float instanceDistanceEnd;
      varying float vLineDistance;

    #endif

    #ifdef USE_COLOR

      varying float vAlpha;

    #endif

    void trimSegment( const in vec4 start, inout vec4 end ) {

      // trim end segment so it terminates between the camera plane and the near plane

      // conservative estimate of the near plane
      float a = projectionMatrix[ 2 ][ 2 ]; // 3nd entry in 3th column
      float b = projectionMatrix[ 3 ][ 2 ]; // 3nd entry in 4th column
      float nearEstimate = - 0.5 * b / a;

      float alpha = ( nearEstimate - start.z ) / ( end.z - start.z );

      end.xyz = mix( start.xyz, end.xyz, alpha );

    }

    void main() {

      #ifdef USE_COLOR

        vColor.xyz = ( position.y < 0.5 ) ? instanceColorStart.xyz : instanceColorEnd.xyz;
        vAlpha = ( position.y < 0.5 ) ? instanceColorStart.w : instanceColorEnd.w;

      #endif

      #ifdef USE_DASH

        vLineDistance = ( position.y < 0.5 ) ? dashScale * instanceDistanceStart : dashScale * instanceDistanceEnd;
        vUv = uv;

      #endif

      float aspect = resolution.x / resolution.y;

      // camera space
      vec4 start = modelViewMatrix * vec4( instanceStart, 1.0 );
      vec4 end = modelViewMatrix * vec4( instanceEnd, 1.0 );

      #ifdef WORLD_UNITS

        worldStart = start.xyz;
        worldEnd = end.xyz;

      #else

        vUv = uv;

      #endif

      // special case for perspective projection, and segments that terminate either in, or behind, the camera plane
      // clearly the gpu firmware has a way of addressing this issue when projecting into ndc space
      // but we need to perform ndc-space calculations in the shader, so we must address this issue directly
      // perhaps there is a more elegant solution -- WestLangley

      bool perspective = ( projectionMatrix[ 2 ][ 3 ] == - 1.0 ); // 4th entry in the 3rd column

      if ( perspective ) {

        if ( start.z < 0.0 && end.z >= 0.0 ) {

          trimSegment( start, end );

        } else if ( end.z < 0.0 && start.z >= 0.0 ) {

          trimSegment( end, start );

        }

      }

      // clip space
      vec4 clipStart = projectionMatrix * start;
      vec4 clipEnd = projectionMatrix * end;

      // ndc space
      vec3 ndcStart = clipStart.xyz / clipStart.w;
      vec3 ndcEnd = clipEnd.xyz / clipEnd.w;

      // direction
      vec2 dir = ndcEnd.xy - ndcStart.xy;

      // account for clip-space aspect ratio
      dir.x *= aspect;
      dir = normalize( dir );

      #ifdef WORLD_UNITS

        // get the offset direction as perpendicular to the view vector
        vec3 worldDir = normalize( end.xyz - start.xyz );
        vec3 offset;
        if ( position.y < 0.5 ) {

          offset = normalize( cross( start.xyz, worldDir ) );

        } else {

          offset = normalize( cross( end.xyz, worldDir ) );

        }

        // sign flip
        if ( position.x < 0.0 ) offset *= - 1.0;

        float forwardOffset = dot( worldDir, vec3( 0.0, 0.0, 1.0 ) );

        // don't extend the line if we're rendering dashes because we
        // won't be rendering the endcaps
        #ifndef USE_DASH

          // extend the line bounds to encompass  endcaps
          start.xyz += - worldDir * linewidth * 0.5;
          end.xyz += worldDir * linewidth * 0.5;

          // shift the position of the quad so it hugs the forward edge of the line
          offset.xy -= dir * forwardOffset;
          offset.z += 0.5;

        #endif

        // endcaps
        if ( position.y > 1.0 || position.y < 0.0 ) {

          offset.xy += dir * 2.0 * forwardOffset;

        }

        // adjust for linewidth
        offset *= linewidth * 0.5;

        // set the world position
        worldPos = ( position.y < 0.5 ) ? start : end;
        worldPos.xyz += offset;

        // project the worldpos
        vec4 clip = projectionMatrix * worldPos;

        // shift the depth of the projected points so the line
        // segements overlap neatly
        vec3 clipPose = ( position.y < 0.5 ) ? ndcStart : ndcEnd;
        clip.z = clipPose.z * clip.w;

      #else

        vec2 offset = vec2( dir.y, - dir.x );
        // undo aspect ratio adjustment
        dir.x /= aspect;
        offset.x /= aspect;

        // sign flip
        if ( position.x < 0.0 ) offset *= - 1.0;

        // endcaps
        if ( position.y < 0.0 ) {

          offset += - dir;

        } else if ( position.y > 1.0 ) {

          offset += dir;

        }

        // adjust for linewidth
        offset *= linewidth;

        // adjust for clip-space to screen-space conversion // maybe resolution should be based on viewport ...
        offset /= resolution.y;

        // select end
        vec4 clip = ( position.y < 0.5 ) ? clipStart : clipEnd;

        // back to clip space
        offset *= clip.w;

        clip.xy += offset;

      #endif

      gl_Position = clip;

      vec4 mvPosition = ( position.y < 0.5 ) ? start : end; // this is an approximation

      #include <logdepthbuf_vertex>
      #include <clipping_planes_vertex>
      #include <fog_vertex>

    }
    `,fragmentShader:`
    uniform vec3 diffuse;
    uniform float opacity;
    uniform float linewidth;

    #ifdef USE_DASH

      uniform float dashOffset;
      uniform float dashSize;
      uniform float gapSize;

    #endif

    varying float vLineDistance;

    #ifdef WORLD_UNITS

      varying vec4 worldPos;
      varying vec3 worldStart;
      varying vec3 worldEnd;

      #ifdef USE_DASH

        varying vec2 vUv;

      #endif

    #else

      varying vec2 vUv;

    #endif

    #ifdef USE_COLOR

      varying float vAlpha;

    #endif

    #include <common>
    #include <color_pars_fragment>
    #include <fog_pars_fragment>
    #include <logdepthbuf_pars_fragment>
    #include <clipping_planes_pars_fragment>

    vec2 closestLineToLine(vec3 p1, vec3 p2, vec3 p3, vec3 p4) {

      float mua;
      float mub;

      vec3 p13 = p1 - p3;
      vec3 p43 = p4 - p3;

      vec3 p21 = p2 - p1;

      float d1343 = dot( p13, p43 );
      float d4321 = dot( p43, p21 );
      float d1321 = dot( p13, p21 );
      float d4343 = dot( p43, p43 );
      float d2121 = dot( p21, p21 );

      float denom = d2121 * d4343 - d4321 * d4321;

      float numer = d1343 * d4321 - d1321 * d4343;

      mua = numer / denom;
      mua = clamp( mua, 0.0, 1.0 );
      mub = ( d1343 + d4321 * ( mua ) ) / d4343;
      mub = clamp( mub, 0.0, 1.0 );

      return vec2( mua, mub );

    }

    void main() {

      #include <clipping_planes_fragment>

      #ifdef USE_DASH

        if ( vUv.y < - 1.0 || vUv.y > 1.0 ) discard; // discard endcaps

        if ( mod( vLineDistance + dashOffset, dashSize + gapSize ) > dashSize ) discard; // todo - FIX

      #endif

      #ifdef USE_COLOR

      float alpha = vAlpha;

      #else

      float alpha = opacity;

      #endif

      #ifdef WORLD_UNITS

        // Find the closest points on the view ray and the line segment
        vec3 rayEnd = normalize( worldPos.xyz ) * 1e5;
        vec3 lineDir = worldEnd - worldStart;
        vec2 params = closestLineToLine( worldStart, worldEnd, vec3( 0.0, 0.0, 0.0 ), rayEnd );

        vec3 p1 = worldStart + lineDir * params.x;
        vec3 p2 = rayEnd * params.y;
        vec3 delta = p1 - p2;
        float len = length( delta );
        float norm = len / linewidth;

        #ifndef USE_DASH

          #ifdef USE_ALPHA_TO_COVERAGE

            float dnorm = fwidth( norm );
            alpha = 1.0 - smoothstep( 0.5 - dnorm, 0.5 + dnorm, norm );

          #else

            if ( norm > 0.5 ) {

              discard;

            }

          #endif

        #endif

      #else

        #ifdef USE_ALPHA_TO_COVERAGE

          // artifacts appear on some hardware if a derivative is taken within a conditional
          float a = vUv.x;
          float b = ( vUv.y > 0.0 ) ? vUv.y - 1.0 : vUv.y + 1.0;
          float len2 = a * a + b * b;
          float dlen = fwidth( len2 );

          if ( abs( vUv.y ) > 1.0 ) {

            alpha = 1.0 - smoothstep( 1.0 - dlen, 1.0 + dlen, len2 );

          }

        #else

          if ( abs( vUv.y ) > 1.0 ) {

            float a = vUv.x;
            float b = ( vUv.y > 0.0 ) ? vUv.y - 1.0 : vUv.y + 1.0;
            float len2 = a * a + b * b;

            if ( len2 > 1.0 ) discard;

          }

        #endif

      #endif

      vec4 diffuseColor = vec4( diffuse, alpha );

      #include <logdepthbuf_fragment>
      #include <color_fragment>

      gl_FragColor = vec4( diffuseColor.rgb, alpha );

      #include <tonemapping_fragment>
      #include <encodings_fragment>
      #include <fog_fragment>
      #include <premultiplied_alpha_fragment>

    }
    `};class rt extends r.jyz{constructor(e){super({type:"LineMaterial",uniforms:r.rDY.clone(r.Vj0.line.uniforms),vertexShader:r.Vj0.line.vertexShader,fragmentShader:r.Vj0.line.fragmentShader,clipping:!0});this.isLineMaterial=!0,this.setValues(e)}get color(){return this.uniforms.diffuse.value}set color(e){this.uniforms.diffuse.value=e}get worldUnits(){return"WORLD_UNITS"in this.defines}set worldUnits(e){e?this.defines.WORLD_UNITS="":delete this.defines.WORLD_UNITS}get lineWidth(){return this.uniforms.linewidth.value}set lineWidth(e){this.uniforms.linewidth.value=e}get dashed(){return Boolean("USE_DASH"in this.defines)}set dashed(e){Boolean(e)!==Boolean("USE_DASH"in this.defines)&&(this.needsUpdate=!0),e?this.defines.USE_DASH="":delete this.defines.USE_DASH}get dashScale(){return this.uniforms.dashScale.value}set dashScale(e){this.uniforms.dashScale.value=e}get dashSize(){return this.uniforms.dashSize.value}set dashSize(e){this.uniforms.dashSize.value=e}get dashOffset(){return this.uniforms.dashOffset.value}set dashOffset(e){this.uniforms.dashOffset.value=e}get gapSize(){return this.uniforms.gapSize.value}set gapSize(e){this.uniforms.gapSize.value=e}get resolution(){return this.uniforms.resolution.value}set resolution(e){this.uniforms.resolution.value.copy(e)}}var Ee=y(89380);const ks=new r.Pa4(1,0,0),Js=new r.Pa4(0,1,0),ot=new r.Pa4,ct=new r.Pa4,pe=new r.Pa4;function Qs(s,e){const t=new r._fP(0,0,0,1);ot.copy(s).normalize(),ct.copy(e).normalize();const i=ot.dot(ct);if(i>=1)return t;if(i<1e-6-1){let n=pe.copy(ks).cross(s);qs(n)&&(n=pe.copy(Js).cross(s)),n.normalize(),t.setFromAxisAngle(n,Math.PI)}else{const n=Math.sqrt((1+i)*2),a=1/n;pe.copy(ot).cross(ct),t.x=pe.x*a,t.y=pe.y*a,t.z=pe.z*a,t.w=n*.5,t.normalize()}return t}function qs(s){return s.lengthSq()<1e-6*1e-6}function Me(s,e,t=1e-5){return Math.abs(s-e)<t}function ht(s,e){const t=Math.trunc(s*255),i=Math.trunc(e*255);return t===i}function Ce(s,e,t){return Math.max(e,Math.min(t,s))}function Ue(s,e,t){return s+(e-s)*t}function u(s){return s<.04045?s*.0773993808:Math.pow(s*.9478672986+.0521327014,2.4)}function Wt(s){const e=(0,Ee.uZ)(s.r*255,0,255)<<24^(0,Ee.uZ)(s.g*255,0,255)<<16^(0,Ee.uZ)(s.b*255,0,255)<<8^(0,Ee.uZ)(s.a*255,0,255)<<0;return("00000000"+e.toString(16)).slice(-8)}function Le(s,e){return ht(s.r,e.r)&&ht(s.g,e.g)&&ht(s.b,e.b)&&Me(s.a,e.a)}class ei{constructor(){this.materials=new Map,this.outlineMaterial=new r.nls({dithering:!0})}acquire(e,t,i){let n=this.materials.get(e);return n||(n={material:t(),refCount:0,disposer:i},this.materials.set(e,n)),++n.refCount,n.material}release(e){const t=this.materials.get(e);return t?(t.refCount--,t.refCount===0&&(t.disposer(t.material),this.materials.delete(e)),t.refCount):0}update(e){for(const t of this.materials.values())t.material instanceof rt?t.material.resolution=e:t.material instanceof r.jyz&&t.material.uniforms.resolution!=null&&(t.material.uniforms.resolution.value=e)}}const ti={id:s=>"BasicColor-"+Wt(s),create:s=>{const e=new r.vBJ({color:new r.Ilk(s.r,s.g,s.b).convertSRGBToLinear(),dithering:!0});return e.name=ti.id(s),e.opacity=s.a,e.transparent=s.a<1,e.depthWrite=!e.transparent,e},dispose:s=>{s.map?.dispose(),s.lightMap?.dispose(),s.aoMap?.dispose(),s.specularMap?.dispose(),s.alphaMap?.dispose(),s.envMap?.dispose(),s.dispose()}},B={id:s=>"StandardColor-"+Wt(s),create:s=>{const e=new r.Wid({color:new r.Ilk(s.r,s.g,s.b).convertSRGBToLinear(),metalness:0,roughness:1,dithering:!0});return e.name=B.id(s),e.opacity=s.a,e.transparent=s.a<1,e.depthWrite=!e.transparent,e},dispose:Yt},De={id:s=>"StandardInstancedColor"+(s?"-t":""),create:s=>{const e=new r.Wid({metalness:0,roughness:1,dithering:!0});return e.name=De.id(s),e.opacity=1,e.transparent=s,e.depthWrite=!e.transparent,e},dispose:Yt},J={id:(s,e)=>`PointsVertexColor-${s.x}x${s.y}${e?"-t":""}`,create:(s,e)=>{const t=new r.UY4({vertexColors:!0,size:s.x,sizeAttenuation:!1});return t.name=J.id(s,e),t.transparent=e,t.depthWrite=!e,t},dispose:s=>{s.map?.dispose(),s.alphaMap?.dispose(),s.dispose()}},be={id:(s,e)=>`LineVertexColorPrepass-${s.toFixed(4)}-${e?"-t":""}`,create:(s,e)=>{const t=new rt({worldUnits:!0,colorWrite:!1,stencilWrite:!0,stencilRef:1,stencilZPass:r.ce8});return t.name=be.id(s,e),t.lineWidth=s,t.transparent=e,t.depthWrite=!e,t},dispose:s=>{s.dispose()}},Pe={id:(s,e)=>`LineVertexColor-${s.toFixed(4)}-${e?"-t":""}`,create:(s,e)=>{const t=new rt({worldUnits:!0,vertexColors:!0,stencilWrite:!0,stencilRef:0,stencilFunc:r.RvT,stencilFail:r.ce8,stencilZPass:r.ce8});return t.name=Pe.id(s,e),t.lineWidth=s,t.transparent=e,t.depthWrite=!e,t},dispose:s=>{s.dispose()}},Be={id:s=>`LineVertexColorPicking-${s.toFixed(4)}`,create:s=>new r.jyz({vertexShader:r.Vj0.line.vertexShader,fragmentShader:`
        uniform vec4 objectId;
        void main() {
          gl_FragColor = objectId;
        }
      `,clipping:!0,uniforms:{objectId:{value:[NaN,NaN,NaN,NaN]},worldUnits:{value:0},linewidth:{value:s},resolution:{value:new r.FM8(1,1)},dashOffset:{value:0},dashScale:{value:1},dashSize:{value:1},gapSize:{value:1}}}),dispose:s=>{s.dispose()}};function Yt(s){s.map?.dispose(),s.lightMap?.dispose(),s.aoMap?.dispose(),s.emissiveMap?.dispose(),s.bumpMap?.dispose(),s.normalMap?.dispose(),s.displacementMap?.dispose(),s.roughnessMap?.dispose(),s.metalnessMap?.dispose(),s.alphaMap?.dispose(),s.envMap?.dispose(),s.dispose()}var si=y(88970);class ii{constructor(e){this.loadModelOptions=e,this._gltfLoader=new si.E,this._models=new Map}async load(e,t){let i=this._models.get(e);return i?await i:(i=this._loadModel(e,this.loadModelOptions).catch(async n=>{t(n)}),this._models.set(e,i),await i)}async _loadModel(e,t){const i=await this._gltfLoader.loadAsync(e);return i.scene.rotateX(Math.PI/2),i}}const me=9,Zt=s=>!0,ni=null;class ai{constructor(e,t,i,n={}){this.materialCache=new Map,this.clearColor=new r.Ilk(16777215),this.currClearColor=new r.Ilk,this.isDebugPass=!1,this.handleAfterRender=()=>{const a=this.gl.renderLists.get(this.scene,0);a.opaque.forEach(this.processItem),a.transmissive.forEach(this.processItem),a.transparent.forEach(this.processItem)},this.processItem=a=>{const o=a.object,c=this.isDebugPass?ri(o.id):o.id,h=a.material,d=a.geometry;if(!d||a.object.userData.picking===!1||!this.shouldPickObjectCB(o))return;const f=o.isInstancedMesh===!0?1:0,p=h.type==="SpriteMaterial"?1:0,l=h.sizeAttenuation===!0?1:0,S=f<<0|p<<1|l<<2;let T=a.object.userData.pickingMaterial??this.materialCache.get(S);if(!T){let G=r.WdD.meshbasic_vert;p===1&&(G=r.WdD.sprite_vert),l===1&&(G=`#define USE_SIZEATTENUATION

`+G),T=new r.jyz({vertexShader:G,fragmentShader:`
           uniform vec4 objectId;
           void main() {
             gl_FragColor = objectId;
           }
         `,side:r.ehD,uniforms:{objectId:{value:[NaN,NaN,NaN,NaN]}}}),this.materialCache.set(S,T)}p===1&&(T.uniforms.rotation={value:h.rotation},T.uniforms.center={value:o.center});const b=T.uniforms.objectId;if(!b)throw new Error("objectId uniform not found in picking material");b.value=[(c>>24&255)/255,(c>>16&255)/255,(c>>8&255)/255,(c&255)/255],T.uniformsNeedUpdate=!0,this.gl.renderBufferDirect(this.camera,ni,d,T,o,null)},this.gl=e,this.scene=t,this.camera=i,this.shouldPickObjectCB=Zt,this.debug=n.debug??!1,this.pickingTarget=new r.dd2(me,me,{minFilter:r.TyD,magFilter:r.TyD,format:r.wk1,encoding:r.rnI}),this.pixelBuffer=new Uint8Array(4*this.pickingTarget.width*this.pickingTarget.height),this.emptyScene=new r.xsS,this.emptyScene.onAfterRender=this.handleAfterRender}dispose(){for(const e of this.materialCache.values())e.dispose();this.materialCache.clear(),this.pickingTarget.dispose()}pick(e,t,i=Zt){this.shouldPickObjectCB=i;const n=Math.floor(me/2),a=this.gl.getPixelRatio(),o=Math.max(0,e*a-n),c=Math.max(0,t*a-n),h=this.gl.domElement.width,d=this.gl.domElement.height;this.camera.setViewOffset(h,d,o,c,me,me);const f=this.gl.getRenderTarget(),p=this.gl.getClearAlpha();this.gl.getClearColor(this.currClearColor),this.gl.setRenderTarget(this.pickingTarget),this.gl.setClearColor(this.clearColor),this.gl.setClearAlpha(1),this.gl.clear(),this.gl.render(this.emptyScene,this.camera),this.gl.readRenderTargetPixels(this.pickingTarget,0,0,this.pickingTarget.width,this.pickingTarget.height,this.pixelBuffer),this.gl.setRenderTarget(f),this.gl.setClearColor(this.currClearColor,p),this.camera.clearViewOffset();const l=Math.min(n,o),m=(Math.min(n,c)*me+l)*4,T=(this.pixelBuffer[m+0]<<24)+(this.pixelBuffer[m+1]<<16)+(this.pixelBuffer[m+2]<<8)+this.pixelBuffer[m+3];return this.debug&&this.pickDebugRender(),T}pickDebugRender(){this.isDebugPass=!0;const e=this.gl.getClearAlpha();this.gl.getClearColor(this.currClearColor),this.gl.setClearColor(this.clearColor),this.gl.setClearAlpha(1),this.gl.clear(),this.gl.render(this.emptyScene,this.camera),this.gl.setClearColor(this.currClearColor,e),this.isDebugPass=!1}}const A=new Uint32Array(1);function ri(s){return A[0]=s|0,A[0]-=A[0]<<6,A[0]^=A[0]>>>17,A[0]-=A[0]<<9,A[0]^=A[0]<<4,A[0]-=A[0]<<3,A[0]^=A[0]<<10,A[0]^=A[0]>>>15,A[0]}var L;(function(s){s[s.Low=0]="Low",s[s.Medium=1]="Medium",s[s.High=2]="High"})(L||(L={}));function oi(s,e){const t=e.maxSamples;switch(s){case L.Low:return Math.min(2,t);case L.Medium:return Math.min(4,t);case L.High:return t}}function Xt(s){switch(s){case L.Low:return 12;case L.Medium:return 20;case L.High:return 32}}function Kt(s){switch(s){case L.Low:return 12;case L.Medium:return 20;case L.High:return 32}}function ci(s){switch(s){case L.Low:return 12;case L.Medium:return 20;case L.High:return 32}}function hi(s){switch(s){case L.Low:return 10;case L.Medium:return 24;case L.High:return 32}}var dt=y(59677),kt=y(11932),di=y(40740),_;(function(s){s[s.ARROW=0]="ARROW",s[s.CUBE=1]="CUBE",s[s.SPHERE=2]="SPHERE",s[s.CYLINDER=3]="CYLINDER",s[s.LINE_STRIP=4]="LINE_STRIP",s[s.LINE_LIST=5]="LINE_LIST",s[s.CUBE_LIST=6]="CUBE_LIST",s[s.SPHERE_LIST=7]="SPHERE_LIST",s[s.POINTS=8]="POINTS",s[s.TEXT_VIEW_FACING=9]="TEXT_VIEW_FACING",s[s.MESH_RESOURCE=10]="MESH_RESOURCE",s[s.TRIANGLE_LIST=11]="TRIANGLE_LIST"})(_||(_={}));var ge;(function(s){s[s.ADD=0]="ADD",s[s.MODIFY=0]="MODIFY",s[s.DELETE=2]="DELETE",s[s.DELETEALL=3]="DELETEALL"})(ge||(ge={}));var j;(function(s){s[s.INT8=1]="INT8",s[s.UINT8=2]="UINT8",s[s.INT16=3]="INT16",s[s.UINT16=4]="UINT16",s[s.INT32=5]="INT32",s[s.UINT32=6]="UINT32",s[s.FLOAT32=7]="FLOAT32",s[s.FLOAT64=8]="FLOAT64"})(j||(j={}));const je=new Set;re(je,"geometry_msgs/TransformStamped");const Ae=new Set;re(Ae,"tf/tfMessage"),re(Ae,"tf2_msgs/TFMessage");const lt=new Set;re(lt,"visualization_msgs/Marker");const ft=new Set;re(ft,"visualization_msgs/MarkerArray");const ut=new Set;re(ut,"nav_msgs/OccupancyGrid");const pt=new Set;re(pt,"sensor_msgs/PointCloud2");function ye(s){return BigInt(s.sec)*BigInt(1e9)+BigInt(s.nsec)}function re(s,e){s.add(e);const t=e.split("/");if(t.length>1){const i=t[0],n=t.slice(1).join("/");s.add(`${i}/msg/${n}`)}return s.add("ros."+e.split("/").join(".")),s}var oe=y(25740),N=y(70159),Re=y(66867);function mt(){return{position:{x:0,y:0,z:0},orientation:{x:0,y:0,z:0,w:1}}}function Jt(){return[0,0,0]}function ga(s,e,t){return[s,e,t]}function ya(s){return[s[0],s[1],s[2]]}function li(){return[0,0,0,1]}function va(s,e,t,i){return[s,e,t,i]}function wa(s){return[s[0],s[1],s[2],s[3]]}function Qt(){return[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]}function Ta(s,e,t,i,n,a,o,c,h,d,f,p,l,S,m,T){return[s,e,t,i,n,a,o,c,h,d,f,p,l,S,m,T]}function Sa(s){return[s[0],s[1],s[2],s[3],s[4],s[5],s[6],s[7],s[8],s[9],s[10],s[11],s[12],s[13],s[14],s[15]]}function gt(s,e,t=1e-5){return Math.abs(s-e)<=t}function fi(s,e){const t=e[0],i=e[1],n=e[2],a=e[4],o=e[5],c=e[6],h=e[8],d=e[9],f=e[10],p=t+o+f;let l=0;return p>0?(l=Math.sqrt(p+1)*2,s[3]=.25*l,s[0]=(c-d)/l,s[1]=(h-n)/l,s[2]=(i-a)/l):t>o&&t>f?(l=Math.sqrt(1+t-o-f)*2,s[3]=(c-d)/l,s[0]=.25*l,s[1]=(i+a)/l,s[2]=(h+n)/l):o>f?(l=Math.sqrt(1+o-t-f)*2,s[3]=(h-n)/l,s[0]=(i+a)/l,s[1]=.25*l,s[2]=(c+d)/l):(l=Math.sqrt(1+f-t-o)*2,s[3]=(i-a)/l,s[0]=(h+n)/l,s[1]=(c+d)/l,s[2]=.25*l),s}const ce=Jt();class he{constructor(e,t){this._position=e,this._rotation=t,oe.Fv(this._rotation,this._rotation),this._matrix=N.yl(Qt(),this._rotation,this._position)}position(){return this._position}rotation(){return this._rotation}matrix(){return this._matrix}setPosition(e){return Re.JG(this._position,e),N.yl(this._matrix,this._rotation,this._position),this}setRotation(e){return oe.Fv(this._rotation,e),N.yl(this._matrix,this._rotation,this._position),this}setPositionRotation(e,t){return Re.JG(this._position,e),oe.Fv(this._rotation,t),N.yl(this._matrix,this._rotation,this._position),this}setPose(e){return Re.t8(this._position,e.position.x,e.position.y,e.position.z),oe.t8(this._rotation,e.orientation.x,e.orientation.y,e.orientation.z,e.orientation.w),oe.Fv(this._rotation,this._rotation),N.yl(this._matrix,this._rotation,this._position),this}setMatrix(e){if(N.Q$(ce,e),!gt(ce[0],1)||!gt(ce[1],1)||!gt(ce[2],1))throw new Error(`setMatrix given a matrix with non-unit scale [${ce[0]}, ${ce[1]}, ${ce[2]}]: ${N.Bd(e)}`);return N.JG(this._matrix,e),N.i0(this._position,e),fi(this._rotation,e),this}copy(e){return Re.JG(this._position,e._position),oe.JG(this._rotation,e._rotation),N.JG(this._matrix,e._matrix),this}toPose(e){e.position.x=this._position[0],e.position.y=this._position[1],e.position.z=this._position[2],e.orientation.x=this._rotation[0],e.orientation.y=this._rotation[1],e.orientation.z=this._rotation[2],e.orientation.w=this._rotation[3]}static Identity(){return new he(Jt(),li())}static Interpolate(e,t,i,n){return Re.t7(e._position,t.position(),i.position(),n),oe.ZA(e._rotation,t.rotation(),i.rotation(),n),e.setPositionRotation(e._position,e._rotation),e}}const yt=mt();function He(s,e,t,i,n,a,o){const c=s.userData.pose;if(!c)throw new Error(`Missing userData.pose for ${s.name}`);const h=Boolean(e.apply(yt,c,t,i,n,a,o));if(s.visible=h,h){const d=yt.position,f=yt.orientation;s.position.set(d.x,d.y,d.z),s.quaternion.set(f.x,f.y,f.z,f.w),s.updateMatrix()}return h}function D(s){switch(s.type){case _.ARROW:case _.CUBE:case _.SPHERE:case _.CYLINDER:case _.TEXT_VIEW_FACING:case _.MESH_RESOURCE:return s.color.a<1;case _.LINE_STRIP:case _.LINE_LIST:case _.CUBE_LIST:case _.SPHERE_LIST:case _.POINTS:case _.TRIANGLE_LIST:default:for(const e of s.colors)if(e.a<1)return!0;return s.colors.length>=s.points.length?!1:s.color.a<1}}function W(s,e){return e.acquire(B.id(s.color),()=>B.create(s.color),B.dispose)}function Y(s,e){e.release(B.id(s.color))}function $e(s,e){const t=D(s);return e.acquire(De.id(t),()=>De.create(t),De.dispose)}function Ve(s,e){const t=D(s);e.release(De.id(t))}function We(s,e){const t=s.scale.x,i=D(s);return e.acquire(be.id(t,i),()=>be.create(t,i),be.dispose)}function Ye(s,e){const t=s.scale.x,i=D(s);e.release(be.id(t,i))}function Ze(s,e){const t=s.scale.x,i=D(s);return e.acquire(Pe.id(t,i),()=>Pe.create(t,i),Pe.dispose)}function Xe(s,e){const t=s.scale.x,i=D(s);e.release(Pe.id(t,i))}function vt(s,e){return e.acquire(Be.id(s),()=>Be.create(s),Be.dispose)}function wt(s,e){e.release(Be.id(s))}function qt(s,e){const t=D(s);return e.acquire(J.id(s.scale,t),()=>J.create(s.scale,t),J.dispose)}function es(s,e){const t=D(s);e.release(J.id(s.scale,t))}const Ke="MISSING_TRANSFORM";function ke(s,e,t){return`Missing transform from frame <${t}> to frame <${s===t?e:s}>`}var ui="../../packages/studio-base/src/panels/ThreeDeeRender/renderables/FrameAxes.ts";const pi=Ge.Z.getLogger(ui),Je=.154,ts=.02,mi=.046,ss=.05,is=new r.Ilk(10238280).convertSRGBToLinear(),ns=new r.Ilk(8969476).convertSRGBToLinear(),as=new r.Ilk(2855163).convertSRGBToLinear(),gi=new r.Ilk(16776960).convertSRGBToLinear(),de=Math.PI/2,rs=6,Q=new r.yGw,H=new r.Pa4,yi=new r.Pa4;class E extends r.Tme{constructor(e){super();this.axesByFrameId=new Map,this.renderer=e,this.lineMaterial=new di.Y({linewidth:2}),this.lineMaterial.color=gi,this.linePickingMaterial=vt(rs,this.renderer.materialCache)}dispose(){for(const e of this.axesByFrameId.values())wi(this.renderer.materialCache),e.userData.shaftMesh.dispose(),e.userData.headMesh.dispose();this.children.length=0,this.axesByFrameId.clear(),this.lineMaterial.dispose(),wt(rs,this.renderer.materialCache)}addTransformMessage(e){const t=!this.renderer.transformTree.hasFrame(e.header.frame_id),i=!this.renderer.transformTree.hasFrame(e.child_frame_id),n=ye(e.header.stamp),a=e.transform.translation,o=e.transform.rotation,c=new he([a.x,a.y,a.z],[o.x,o.y,o.z,o.w]);this.renderer.transformTree.addTransform(e.child_frame_id,e.header.frame_id,n,c),t&&this._addFrameAxis(e.header.frame_id),i&&this._addFrameAxis(e.child_frame_id),(t||i)&&(pi.debug(`Added transform "${e.header.frame_id}_T_${e.child_frame_id}"`),this.renderer.emit("transformTreeUpdated",this.renderer))}startFrame(e){this.lineMaterial.resolution=this.renderer.input.canvasSize;const t=this.renderer.renderFrameId,i=this.renderer.fixedFrameId;if(!(!t||!i)){for(const[n,a]of this.axesByFrameId.entries()){const o=He(a,this.renderer.transformTree,t,i,n,e,e);if(a.visible=o,!o){const c=ke(t,i,n);this.renderer.layerErrors.addToLayer(`f:${n}`,Ke,c)}}for(const[n,a]of this.axesByFrameId.entries()){const o=a.userData.parentLine;if(o){o.visible=!1;const h=this.renderer.transformTree.frame(a.userData.frameId)?.parent();if(h){const d=this.axesByFrameId.get(h.id);if(d?.visible===!0){d.getWorldPosition(H);const f=H.distanceTo(a.getWorldPosition(yi));o.lookAt(H),o.rotateY(-de),o.scale.set(f,1,1),o.visible=!0}}}}}}_addFrameAxis(e){if(this.axesByFrameId.has(e))return;const t=new r.Tme;t.name=e,t.userData.frameId=e,t.userData.pose=mt();const i=vi(this.renderer.materialCache),n=E.ShaftGeometry(this.renderer.maxLod),a=new r.SPe(n,i,3);a.castShadow=!0,a.receiveShadow=!0,t.add(a),H.set(Je,ts,ts),a.setMatrixAt(0,Q.identity().scale(H)),a.setMatrixAt(1,Q.makeRotationZ(de).scale(H)),a.setMatrixAt(2,Q.makeRotationY(-de).scale(H)),a.setColorAt(0,is),a.setColorAt(1,ns),a.setColorAt(2,as);const o=E.HeadGeometry(this.renderer.maxLod),c=new r.SPe(o,i,3);c.castShadow=!0,c.receiveShadow=!0,t.add(c),H.set(mi,ss,ss),Q.identity().scale(H).setPosition(Je,0,0),c.setMatrixAt(0,Q),Q.makeRotationZ(de).scale(H).setPosition(0,Je,0),c.setMatrixAt(1,Q),Q.makeRotationY(-de).scale(H).setPosition(0,0,Je),c.setMatrixAt(2,Q),c.setColorAt(0,is),c.setColorAt(1,ns),c.setColorAt(2,as),t.userData.shaftMesh=a,t.userData.headMesh=c;const h=this.renderer.transformTree.frame(e);if(!h)throw new Error(`CoordinateFrame "${e}" was not created`);const d=h.parent();d&&this.axesByFrameId.get(d.id)&&this._addChildParentLine(t);for(const f of this.renderer.transformTree.frames().values())if(f.parent()===h){const p=this.axesByFrameId.get(f.id);p&&this._addChildParentLine(p)}this.add(t),this.axesByFrameId.set(e,t)}_addChildParentLine(e){e.userData.parentLine&&e.remove(e.userData.parentLine);const t=new dt.w(E.LineGeometry(),this.lineMaterial);t.castShadow=!0,t.receiveShadow=!1,t.userData.pickingMaterial=this.linePickingMaterial,e.add(t),e.userData.parentLine=t}static ShaftGeometry(e){if(!E.shaftGeometry||e!==E.shaftLod){const t=Xt(e);E.shaftGeometry=new r.fHI(.5,.5,1,t,1,!1),E.shaftGeometry.rotateZ(-de),E.shaftGeometry.translate(.5,0,0),E.shaftGeometry.computeBoundingSphere(),E.shaftLod=e}return E.shaftGeometry}static HeadGeometry(e){if(!E.headGeometry||e!==E.headLod){const t=Kt(e);E.headGeometry=new r.b_z(.5,1,t,1,!1),E.headGeometry.rotateZ(-de),E.headGeometry.translate(.5,0,0),E.headGeometry.computeBoundingSphere(),E.headLod=e}return E.headGeometry}static LineGeometry(){return E.lineGeometry||(E.lineGeometry=new kt.L,E.lineGeometry.setPositions([0,0,0,1,0,0])),E.lineGeometry}}const Tt={r:1,g:1,b:1,a:1};function vi(s){return s.acquire(B.id(Tt),()=>B.create(Tt),B.dispose)}function wi(s){s.release(B.id(Tt))}function Z(s,e,t){return`${s}:${e?e+":":""}${t}`.replace(/\s/g,"_")}const Qe=new r.Ilk,Ne=[0,0,0,0];class X extends r.Tme{constructor(e,t,i){super();this._renderer=i,this.name=Z(e,t.ns,t.id),this.userData={topic:e,marker:t,pose:t.pose,srcTime:ye(t.header.stamp)},i.renderables.set(this.name,this)}dispose(){this._renderer.renderables.delete(this.name)}update(e){this.userData.marker=e,this.userData.srcTime=ye(e.header.stamp),this.userData.pose=e.pose}_markerColorsToLinear(e,t){const i=e.points.length;for(let n=0;n<i;n++){const a=e.colors[n]??e.color;Qe.setRGB(a.r,a.g,a.b).convertSRGBToLinear(),Ne[0]=Qe.r,Ne[1]=Qe.g,Ne[2]=Qe.b,Ne[3]=a.a,t(Ne,n)}}}const os=.77,cs=1,hs=.23,ds=2,Ti=.23,Si=new r.Pa4(0,0,-1),ls=new r.Pa4,fs=new r.Pa4,qe=new r.Pa4;class v extends X{constructor(e,t,i){super(e,t,i);const n=W(t,i.materialCache);this.shaftMesh=new r.Kj0(v.shaftGeometry(i.maxLod),n),this.shaftMesh.castShadow=!0,this.shaftMesh.receiveShadow=!0,this.add(this.shaftMesh),this.headMesh=new r.Kj0(v.headGeometry(i.maxLod),n),this.headMesh.castShadow=!0,this.headMesh.receiveShadow=!0,this.add(this.headMesh),this.shaftOutline=new r.ejS(v.shaftEdgesGeometry(i.maxLod),i.materialCache.outlineMaterial),this.shaftOutline.userData.picking=!1,this.shaftMesh.add(this.shaftOutline),this.headOutline=new r.ejS(v.headEdgesGeometry(i.maxLod),i.materialCache.outlineMaterial),this.headOutline.userData.picking=!1,this.headMesh.add(this.headOutline),this.update(t)}dispose(){Y(this.userData.marker,this._renderer.materialCache)}update(e){const t=this.userData.marker;if(super.update(e),Le(e.color,t.color)||(Y(t,this._renderer.materialCache),this.shaftMesh.material=W(e,this._renderer.materialCache),this.headMesh.material=this.shaftMesh.material),e.points.length===2){const i=e.points[0],n=e.points[1];ls.set(i.x,i.y,i.z),fs.set(n.x,n.y,n.z),qe.subVectors(fs,ls);const a=qe.length();let o=Ti*a;if(e.scale.z!==0){const f=e.scale.z;o=(0,Ee.uZ)(f,0,a)}const c=a-o,h=e.scale.x,d=e.scale.y;this.shaftMesh.scale.set(c,h,h),this.headMesh.scale.set(o,d,d),this.scale.set(1,1,1),this.shaftMesh.position.set(0,0,0),qe.normalize(),xi(i,this.userData.pose.position),this.userData.pose.orientation=Qs(Si,qe)}else{this.shaftMesh.scale.set(os,cs,cs),this.headMesh.scale.set(hs,ds,ds),this.scale.set(e.scale.x,e.scale.y,e.scale.z);const i=os/2,n=hs/2;this.shaftMesh.position.set(i,0,0),this.headMesh.position.set(i*2+n,0,0)}}static shaftGeometry(e){if(!v._shaftGeometry||e!==v._shaftLod){const t=Xt(e);v._shaftGeometry=new r.fHI(.5,.5,1,t,1,!1),v._shaftGeometry.rotateZ(-Math.PI/2),v._shaftGeometry.computeBoundingSphere(),v._shaftLod=e}return v._shaftGeometry}static headGeometry(e){if(!v._headGeometry||e!==v._headLod){const t=Kt(e);v._headGeometry=new r.b_z(.5,1,t,1,!1),v._headGeometry.rotateZ(-Math.PI/2),v._headGeometry.computeBoundingSphere(),v._headLod=e}return v._headGeometry}static shaftEdgesGeometry(e){if(!v._shaftEdgesGeometry){const t=v.shaftGeometry(e);v._shaftEdgesGeometry=new r.TOt(t,40),v._shaftEdgesGeometry.computeBoundingSphere()}return v._shaftEdgesGeometry}static headEdgesGeometry(e){if(!v._headEdgesGeometry){const t=v.headGeometry(e);v._headEdgesGeometry=new r.TOt(t,40),v._headEdgesGeometry.computeBoundingSphere()}return v._headEdgesGeometry}}function xi(s,e){e.x=s.x,e.y=s.y,e.z=s.z}class O extends X{constructor(e,t,i){super(e,t,i);this.mesh=new r.Kj0(O.geometry(),W(t,i.materialCache)),this.mesh.castShadow=!0,this.mesh.receiveShadow=!0,this.add(this.mesh),this.outline=new r.ejS(O.edgesGeometry(),i.materialCache.outlineMaterial),this.outline.userData.picking=!1,this.mesh.add(this.outline),this.update(t)}dispose(){Y(this.userData.marker,this._renderer.materialCache)}update(e){const t=this.userData.marker;super.update(e),Le(e.color,t.color)||(Y(t,this._renderer.materialCache),this.mesh.material=W(e,this._renderer.materialCache)),this.scale.set(e.scale.x,e.scale.y,e.scale.z)}static geometry(){return O._geometry||(O._geometry=new r.DvJ(1,1,1),O._geometry.computeBoundingSphere()),O._geometry}static edgesGeometry(){return O._edgesGeometry||(O._edgesGeometry=new r.TOt(O.geometry(),40),O._edgesGeometry.computeBoundingSphere()),O._edgesGeometry}}const _i=4,St=new r.yGw,us=new r.Pa4,ps=new r.Ilk;class ms extends r.SPe{constructor(e,t,i=_i){super(e,t,0);this._capacity=i,this._resize()}set(e,t,i,n){const a=e.length;this._setCount(a);for(let o=0;o<a;o++){const c=e[o],h=i[o]??n;St.makeTranslation(c.x,c.y,c.z),us.set(t.x,t.y,t.z),St.scale(us),ps.setRGB(h.r,h.g,h.b),this.setMatrixAt(o,St),this.setColorAt(o,ps)}}_setCount(e){for(;e>=this._capacity;)this._expand();this.count=e,this.instanceMatrix.count=e,this.instanceColor.count=e}_expand(){this._capacity=this._capacity+Math.trunc(this._capacity/2)+16,this._resize()}_resize(){const e=this.instanceMatrix.array,t=this.instanceColor?.array,i=new Float32Array(this._capacity*16),n=new Float32Array(this._capacity*3);e.length>0&&i.set(e),t&&t.length>0&&n.set(t),this.instanceMatrix=new r.lb7(i,16),this.instanceColor=new r.lb7(n,3),this.instanceMatrix.setUsage(r.dj0),this.instanceColor.setUsage(r.dj0)}}class Ii extends X{constructor(e,t,i){super(e,t,i);const n=$e(t,i.materialCache);this.mesh=new ms(O.geometry(),n,t.points.length),this.mesh.castShadow=!0,this.mesh.receiveShadow=!0,this.add(this.mesh),this.update(t)}dispose(){Ve(this.userData.marker,this._renderer.materialCache)}update(e){const t=this.userData.marker;super.update(e),D(e)!==D(t)&&(Ve(t,this._renderer.materialCache),this.mesh.material=$e(e,this._renderer.materialCache)),this.mesh.set(e.points,e.scale,e.colors,e.color)}}class R extends X{constructor(e,t,i){super(e,t,i);const n=W(t,i.materialCache);this.mesh=new r.Kj0(R.geometry(i.maxLod),n),this.mesh.castShadow=!0,this.mesh.receiveShadow=!0,this.add(this.mesh),this.outline=new r.ejS(R.edgesGeometry(i.maxLod),i.materialCache.outlineMaterial),this.outline.userData.picking=!1,this.mesh.add(this.outline),this.update(t)}dispose(){Y(this.userData.marker,this._renderer.materialCache)}update(e){const t=this.userData.marker;super.update(e),Le(e.color,t.color)||(Y(t,this._renderer.materialCache),this.mesh.material=W(e,this._renderer.materialCache)),this.scale.set(e.scale.x,e.scale.y,e.scale.z)}static geometry(e){if(!R._geometry||e!==R._lod){const t=ci(e);R._geometry=new r.fHI(.5,.5,1,t),R._geometry.rotateX(Math.PI/2),R._geometry.computeBoundingSphere(),R._lod=e}return R._geometry}static edgesGeometry(e){if(!R._edgesGeometry){const t=R.geometry(e);R._edgesGeometry=new r.TOt(t,40),R._edgesGeometry.computeBoundingSphere()}return R._edgesGeometry}}var gs=y(8052),Ei=y(16896);const ys=6;class Mi extends X{constructor(e,t,i){super(e,t,i);this.geometry=new Ei.z;const n=We(t,i.materialCache);this.linePrepass=new gs.w(this.geometry,n),this.linePrepass.renderOrder=1,this.linePrepass.userData.picking=!1,this.add(this.linePrepass);const a=Ze(t,i.materialCache);this.line=new gs.w(this.geometry,a),this.line.renderOrder=2;const o=Math.max(t.scale.x,ys);this.line.userData.pickingMaterial=vt(o,i.materialCache),this.add(this.line),this.update(t)}dispose(){Ye(this.userData.marker,this._renderer.materialCache),Xe(this.userData.marker,this._renderer.materialCache);const e=Math.max(this.userData.marker.scale.x,ys);wt(e,this._renderer.materialCache),this.line.userData.pickingMaterial=void 0}update(e){const t=this.userData.marker;super.update(e);const i=t.scale.x,n=D(t),a=e.scale.x,o=D(e);(!Me(i,a)||n!==o)&&(Ye(t,this._renderer.materialCache),Xe(t,this._renderer.materialCache),this.linePrepass.material=We(e,this._renderer.materialCache),this.line.material=Ze(e,this._renderer.materialCache)),this._setPositions(e),this._setColors(e),this.linePrepass.computeLineDistances(),this.line.computeLineDistances()}_setPositions(e){const t=new Float32Array(3*e.points.length);for(let i=0;i<e.points.length;i++){const n=e.points[i];t[i*3+0]=n.x,t[i*3+1]=n.y,t[i*3+2]=n.z}this.geometry.setPositions(t)}_setColors(e){const t=new Float32Array(4*e.points.length);this._markerColorsToLinear(e,(n,a)=>{t[4*a+0]=n[0],t[4*a+1]=n[1],t[4*a+2]=n[2],t[4*a+3]=n[3]});const i=new r.$TI(t,8,1);this.geometry.setAttribute("instanceColorStart",new r.kB5(i,4,0)),this.geometry.setAttribute("instanceColorEnd",new r.kB5(i,4,4))}}const vs=6;class Ci extends X{constructor(e,t,i){super(e,t,i);this.geometry=new kt.L;const n=We(t,i.materialCache);this.linePrepass=new dt.w(this.geometry,n),this.linePrepass.renderOrder=1,this.linePrepass.userData.picking=!1,this.add(this.linePrepass);const a=Ze(t,i.materialCache);this.line=new dt.w(this.geometry,a),this.line.renderOrder=2;const o=Math.max(t.scale.x,vs);this.line.userData.pickingMaterial=vt(o,i.materialCache),this.add(this.line),this.update(t)}dispose(){Ye(this.userData.marker,this._renderer.materialCache),Xe(this.userData.marker,this._renderer.materialCache);const e=Math.max(this.userData.marker.scale.x,vs);wt(e,this._renderer.materialCache),this.line.userData.pickingMaterial=void 0}update(e){const t=this.userData.marker;super.update(e);const i=t.scale.x,n=D(t),a=e.scale.x,o=D(e);(!Me(i,a)||n!==o)&&(Ye(t,this._renderer.materialCache),Xe(t,this._renderer.materialCache),this.linePrepass.material=We(e,this._renderer.materialCache),this.line.material=Ze(e,this._renderer.materialCache)),this._setPositions(e),this._setColors(e),this.linePrepass.computeLineDistances(),this.line.computeLineDistances()}_setPositions(e){const t=new Float32Array(3*e.points.length);for(let i=0;i<e.points.length;i++){const n=e.points[i];t[i*3+0]=n.x,t[i*3+1]=n.y,t[i*3+2]=n.z}this.geometry.setPositions(t)}_setColors(e){const t=new Float32Array(8*e.points.length),i=[0,0,0,0];this._markerColorsToLinear(e,(a,o)=>{if(o===0){ws(a,i);return}const c=o-1;t[8*c+0]=i[0],t[8*c+1]=i[1],t[8*c+2]=i[2],t[8*c+3]=i[3],t[8*c+4]=a[0],t[8*c+5]=a[1],t[8*c+6]=a[2],t[8*c+7]=a[3],ws(a,i)});const n=new r.$TI(t,8,1);this.geometry.setAttribute("instanceColorStart",new r.kB5(n,4,0)),this.geometry.setAttribute("instanceColorEnd",new r.kB5(n,4,4))}}function ws(s,e){e[0]=s[0],e[1]=s[1],e[2]=s[2],e[3]=s[3]}const Li="MESH_FETCH_FAILED";class Di extends X{constructor(e,t,i){super(e,t,i);this.material=W(t,i.materialCache),this._loadModel(t.mesh_resource,{useEmbeddedMaterials:t.mesh_use_embedded_materials}).catch(()=>{}),this.update(t)}dispose(){Y(this.userData.marker,this._renderer.materialCache)}update(e){const t=this.userData.marker;super.update(e),Le(e.color,t.color)||(Y(t,this._renderer.materialCache),this.material=W(e,this._renderer.materialCache)),e.mesh_resource!==t.mesh_resource&&this._loadModel(e.mesh_resource,{useEmbeddedMaterials:e.mesh_use_embedded_materials}).catch(()=>{}),this.scale.set(e.scale.x,e.scale.y,e.scale.z)}async _loadModel(e,t){this.mesh&&(this.remove(this.mesh),this.mesh=void 0);const i=await this._renderer.modelCache.load(e,o=>{this._renderer.layerErrors.addToTopic(this.userData.topic,Li,`Failed to load mesh resource from "${e}": ${o.message}`)});if(!i)return;const n=i.scene.clone(!0),a=[];n.traverse(o=>{if(!(o instanceof r.Kj0))return;o.castShadow=!0,o.receiveShadow=!0;const c=new r.TOt(o.geometry,40),h=new r.ejS(c,this._renderer.materialCache.outlineMaterial);if(a.push([h,o]),!t.useEmbeddedMaterials){const d=o;if(Array.isArray(d.material))for(const f of d.material)B.dispose(f);else B.dispose(d.material);d.material=this.material}});for(const[o,c]of a)c.add(o);this.mesh=n,this.add(this.mesh)}}class Ts extends r.u9r{constructor(e){super();this.attributes={},this._itemCapacity=0,this._dataConstructor=e}createAttribute(e,t){const i=new this._dataConstructor(this._itemCapacity*t),n=new r.TlE(i,t);return n.setUsage(r.dj0),this.setAttribute(e,n)}resize(e){if(this.setDrawRange(0,e),e<=this._itemCapacity){for(const t of Object.values(this.attributes))t.count=e;return}for(const t of Object.keys(this.attributes)){const i=this.attributes[t],n=new this._dataConstructor(e*i.itemSize),a=new r.TlE(n,i.itemSize,i.normalized);a.setUsage(r.dj0),this.attributes[t]=a}this._itemCapacity=e}}class bi extends X{constructor(e,t,i){super(e,t,i);this.geometry=new Ts(Float32Array),this.geometry.createAttribute("position",3),this.geometry.createAttribute("color",4);const n=qt(t,i.materialCache);this.points=new r.woe(this.geometry,n),this.add(this.points),this.update(t)}dispose(){es(this.userData.marker,this._renderer.materialCache)}update(e){const t=this.userData.marker;super.update(e);const i=t.scale.x,n=t.scale.y,a=D(t),o=e.scale.x,c=e.scale.y,h=D(e);(!Me(i,o)||!Me(n,c)||a!==h)&&(es(t,this._renderer.materialCache),this.points.material=qt(e,this._renderer.materialCache)),this.geometry.resize(e.points.length),this._setPositions(e),this._setColors(e)}_setPositions(e){const t=this.geometry.getAttribute("position"),i=t.array;for(let n=0;n<e.points.length;n++){const a=e.points[n];i[n*3+0]=a.x,i[n*3+1]=a.y,i[n*3+2]=a.z}t.needsUpdate=!0}_setColors(e){const t=this.geometry.getAttribute("color"),i=t.array;this._markerColorsToLinear(e,(n,a)=>{i[4*a+0]=n[0],i[4*a+1]=n[1],i[4*a+2]=n[2],i[4*a+3]=n[3]}),t.needsUpdate=!0}}class K extends X{constructor(e,t,i){super(e,t,i);const n=W(t,i.materialCache);this.mesh=new r.Kj0(K.geometry(i.maxLod),n),this.mesh.castShadow=!0,this.mesh.receiveShadow=!0,this.add(this.mesh),this.update(t)}dispose(){Y(this.userData.marker,this._renderer.materialCache)}update(e){const t=this.userData.marker;super.update(e),Le(e.color,t.color)||(Y(t,this._renderer.materialCache),this.mesh.material=W(e,this._renderer.materialCache)),this.scale.set(e.scale.x,e.scale.y,e.scale.z)}static geometry(e){if(!K._geometry||e!==K._lod){const t=hi(e);K._geometry=new r.xo$(.5,t,t),K._geometry.computeBoundingSphere(),K._lod=e}return K._geometry}}class Pi extends X{constructor(e,t,i){super(e,t,i);const n=$e(t,i.materialCache);this.mesh=new ms(K.geometry(i.maxLod),n,t.points.length),this.mesh.castShadow=!0,this.mesh.receiveShadow=!0,this.add(this.mesh),this.update(t)}dispose(){Ve(this.userData.marker,this._renderer.materialCache)}update(e){const t=this.userData.marker;super.update(e),D(e)!==D(t)&&(Ve(t,this._renderer.materialCache),this.mesh.material=$e(e,this._renderer.materialCache)),this.mesh.set(e.points,e.scale,e.colors,e.color)}}const Ai="INVALID_CUBE_LIST",xt="INVALID_LINE_LIST",Ss="INVALID_LINE_STRIP",Ri="INVALID_MARKER_ACTION",Ni="INVALID_MARKER_TYPE",Oi="INVALID_POINTS_LIST",zi="INVALID_SPHERE_LIST";class Gi extends r.Tme{constructor(e,t){super();this.namespaces=new Map,this.topic=e,this.renderer=t}dispose(){}addMarkerMessage(e){switch(e.action){case ge.ADD:case ge.MODIFY:this._addOrUpdateMarker(e);break;case ge.DELETE:{const t=this.namespaces.get(e.ns);if(t){const i=t.get(e.id);i&&(this.remove(i),i.dispose(),t.delete(e.id))}break}case ge.DELETEALL:{for(const t of this.namespaces.values())for(const i of t.values())this.remove(i),i.dispose();this.namespaces.clear();break}default:this.renderer.layerErrors.addToTopic(this.topic,Ri,`Invalid marker action ${e.action}`)}}startFrame(e,t,i){for(const n of this.namespaces.values())for(const a of n.values()){const o=a.userData.marker,c=o.header.frame_id,h=o.frame_locked?e:a.userData.srcTime,d=He(a,this.renderer.transformTree,t,i,c,e,h);if(a.visible=d,!d){const f=ke(t,i,c);this.renderer.layerErrors.addToTopic(a.userData.topic,Ke,f)}}}_addOrUpdateMarker(e){let t=this.namespaces.get(e.ns);t||(t=new Map,this.namespaces.set(e.ns,t));let i=t.get(e.id);if(i)i.update(e);else{if(i=this._createMarkerRenderable(e),!i)return;this.add(i),t.set(e.id,i)}}_createMarkerRenderable(e){switch(e.type){case _.ARROW:return new v(this.topic,e,this.renderer);case _.CUBE:return new O(this.topic,e,this.renderer);case _.SPHERE:return new K(this.topic,e,this.renderer);case _.CYLINDER:return new R(this.topic,e,this.renderer);case _.LINE_STRIP:if(e.points.length===0){const t=Z(this.topic,e.ns,e.id);this.renderer.layerErrors.addToTopic(this.topic,Ss,`LINE_STRIP marker ${t} has no points`);return}else if(e.points.length===1){const t=Z(this.topic,e.ns,e.id);this.renderer.layerErrors.addToTopic(this.topic,Ss,`LINE_STRIP marker ${t} only has one point`);return}return new Ci(this.topic,e,this.renderer);case _.LINE_LIST:if(e.points.length===0){const t=Z(this.topic,e.ns,e.id);this.renderer.layerErrors.addToTopic(this.topic,xt,`LINE_LIST marker ${t} has no points`)}else if(e.points.length===1){const t=Z(this.topic,e.ns,e.id);this.renderer.layerErrors.addToTopic(this.topic,xt,`LINE_LIST marker ${t} only has one point`);return}else if(e.points.length%2!==0){const t=Z(this.topic,e.ns,e.id);this.renderer.layerErrors.addToTopic(this.topic,xt,`LINE_LIST marker ${t} has an odd number of points (${e.points.length})`);return}return new Mi(this.topic,e,this.renderer);case _.CUBE_LIST:if(e.points.length===0){const t=Z(this.topic,e.ns,e.id);this.renderer.layerErrors.addToTopic(this.topic,Ai,`CUBE_LIST marker ${t} has no points`);return}return new Ii(this.topic,e,this.renderer);case _.SPHERE_LIST:if(e.points.length===0){const t=Z(this.topic,e.ns,e.id);this.renderer.layerErrors.addToTopic(this.topic,zi,`SPHERE_LIST marker ${t} has no points`);return}return new Pi(this.topic,e,this.renderer);case _.POINTS:if(e.points.length===0){const t=Z(this.topic,e.ns,e.id);this.renderer.layerErrors.addToTopic(this.topic,Oi,`POINTS marker ${t} has no points`);return}return new bi(this.topic,e,this.renderer);case _.TEXT_VIEW_FACING:return;case _.MESH_RESOURCE:return new Di(this.topic,e,this.renderer);case _.TRIANGLE_LIST:return;default:{const t=Z(this.topic,e.ns,e.id);this.renderer.layerErrors.addToTopic(this.topic,Ni,`Marker ${t} has invalid type ${e.type}`);return}}}}class Fi extends r.Tme{constructor(e){super();this.topics=new Map,this.renderer=e}dispose(){for(const e of this.topics.values())e.dispose();this.topics.clear()}addMarkerMessage(e,t){let i=this.topics.get(e);i||(i=new Gi(e,this.renderer),this.topics.set(e,i),this.add(i)),i.addMarkerMessage(t)}startFrame(e){const t=this.renderer.renderFrameId,i=this.renderer.fixedFrameId;if(!(!t||!i))for(const n of this.topics.values())n.startFrame(e,t,i)}}const Ui="INVALID_OCCUPANCY_GRID",Bi={r:1,g:1,b:1,a:.5},ji={r:0,g:0,b:0,a:.5},Hi={r:.5,g:.5,b:.5,a:.5},$i={r:1,g:0,b:1,a:1};class q extends r.Tme{constructor(e){super();this.occupancyGridsByTopic=new Map,this.renderer=e}dispose(){for(const e of this.occupancyGridsByTopic.values())e.userData.texture.dispose(),e.userData.material.dispose(),e.userData.mesh.userData.pickingMaterial.dispose();this.children.length=0,this.occupancyGridsByTopic.clear()}addOccupancyGridMessage(e,t){let i=this.occupancyGridsByTopic.get(e);if(!i){i=new r.Tme,i.name=e,i.userData.topic=e,i.userData.settings={minColor:Bi,maxColor:ji,unknownColor:Hi,invalidColor:$i,frameLocked:!0},i.userData.occupancyGrid=t,i.userData.pose=t.info.origin,i.userData.srcTime=ye(t.header.stamp);const n=xs(t),a=Yi(n,i),o=new r.Kj0(q.Geometry(),a);o.userData.pickingMaterial=Zi(n),o.castShadow=!0,o.receiveShadow=!0,i.userData.texture=n,i.userData.material=a,i.userData.mesh=o,i.add(i.userData.mesh),this.add(i),this.occupancyGridsByTopic.set(e,i)}this._updateOccupancyGridRenderable(i,t)}startFrame(e){const t=this.renderer.renderFrameId,i=this.renderer.fixedFrameId;if(!(!t||!i))for(const n of this.occupancyGridsByTopic.values()){const o=n.userData.settings.frameLocked?e:n.userData.srcTime,c=n.userData.occupancyGrid.header.frame_id;if(!He(n,this.renderer.transformTree,t,i,c,e,o)){const d=ke(t,i,c);this.renderer.layerErrors.addToTopic(n.userData.topic,Ke,d)}}}_updateOccupancyGridRenderable(e,t){const i=t.info.width*t.info.height;if(t.data.length!==i){const h=`OccupancyGrid data length (${t.data.length}) is not equal to width ${t.info.width} * height ${t.info.height}`;Vi(this.renderer,e,h);return}let n=e.userData.texture;const a=t.info.width,o=t.info.height,c=t.info.resolution;(a!==n.image.width||o!==n.image.height)&&(n.dispose(),n=xs(t),e.userData.texture=n,e.userData.material.map=n),Wi(n,t,e.userData.settings),e.scale.set(c*a,c*o,1)}static Geometry(){return q.geometry||(q.geometry=new r._12(1,1,1,1),q.geometry.translate(.5,.5,0),q.geometry.computeBoundingSphere()),q.geometry}}function Vi(s,e,t){s.layerErrors.addToTopic(e.userData.topic,Ui,t),e.userData.positionAttribute.resize(0),e.userData.colorAttribute.resize(0)}function xs(s){const e=s.info.width,t=s.info.height,i=e*t,n=new Uint8ClampedArray(i*4);return new r.IEO(n,e,t,r.wk1,r.ywz,r.xfE,r.uWy,r.uWy,r.TyD,r.TyD,1,r.rnI)}const ee={r:0,g:0,b:0,a:0},te={r:0,g:0,b:0,a:0},F={r:0,g:0,b:0,a:0},se={r:0,g:0,b:0,a:0};function Wi(s,e,t){const i=e.info.width*e.info.height,n=s.image.data;ee.r=u(t.unknownColor.r)*255,ee.g=u(t.unknownColor.g)*255,ee.b=u(t.unknownColor.b)*255,ee.a=t.unknownColor.a*255,te.r=u(t.invalidColor.r)*255,te.g=u(t.invalidColor.g)*255,te.b=u(t.invalidColor.b)*255,te.a=t.invalidColor.a*255,F.r=u(t.minColor.r)*255,F.g=u(t.minColor.g)*255,F.b=u(t.minColor.b)*255,F.a=t.minColor.a*255,se.r=u(t.maxColor.r)*255,se.g=u(t.maxColor.g)*255,se.b=u(t.maxColor.b)*255,se.a=t.maxColor.a*255;const a=e.data;for(let o=0;o<i;o++){const c=a[o]|0,h=o*4;if(c===-1)n[h+0]=ee.r,n[h+1]=ee.g,n[h+2]=ee.b,n[h+3]=ee.a;else if(c>=0&&c<=100){const d=c/100;d===1?(n[h+0]=0,n[h+1]=0,n[h+2]=0,n[h+3]=0):(n[h+0]=F.r+(se.r-F.r)*d,n[h+1]=F.g+(se.g-F.g)*d,n[h+2]=F.b+(se.b-F.b)*d,n[h+3]=F.a+(se.a-F.a)*d)}else n[h+0]=te.r,n[h+1]=te.g,n[h+2]=te.b,n[h+3]=te.a}s.needsUpdate=!0}function Yi(s,e){const t=Xi(e.userData.settings),i=new r.vBJ({map:s,side:r.ehD});return i.name=`${e.userData.topic}:Material`,i.transparent=t,i.depthWrite=!i.transparent,i}function Zi(s){return new r.jyz({vertexShader:`
      varying vec2 vUv;
      void main() {
        vUv = uv;
        gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
      }
    `,fragmentShader:`
      uniform sampler2D map;
      uniform vec4 objectId;
      varying vec2 vUv;
      void main() {
        vec4 color = texture2D(map, vUv);
        if (color.a == 0.0) {
          discard;
        }
        gl_FragColor = objectId;
      }
    `,side:r.ehD,uniforms:{map:{value:s},objectId:{value:[NaN,NaN,NaN,NaN]}}})}function Xi(s){return s.minColor.a<1||s.maxColor.a<1||s.invalidColor.a<1||s.unknownColor.a<1}var M;(function(s){s[s.Flat=0]="Flat",s[s.Gradient=1]="Gradient",s[s.Rainbow=2]="Rainbow",s[s.Rgb=3]="Rgb",s[s.Rgba=4]="Rgba",s[s.Turbo=5]="Turbo"})(M||(M={}));function Ki(s,e,t){const i=t-e;switch(s.colorMode){case M.Flat:return(n,a)=>{n.r=u(s.flatColor.r),n.g=u(s.flatColor.g),n.b=u(s.flatColor.b),n.a=s.flatColor.a};case M.Gradient:return(n,a)=>{const o=(a-e)/i;n.r=u(Ue(s.minColor.r,s.maxColor.r,o)),n.g=u(Ue(s.minColor.g,s.maxColor.g,o)),n.b=u(Ue(s.minColor.b,s.maxColor.b,o)),n.a=Ue(s.minColor.a,s.maxColor.a,o)};case M.Rainbow:return(n,a)=>{const o=(a-e)/i;sn(n,o)};case M.Rgb:switch(s.rgbByteOrder){default:case"rgba":return ki;case"bgra":return Qi;case"abgr":return en}case M.Rgba:switch(s.rgbByteOrder){default:case"rgba":return Ji;case"bgra":return qi;case"abgr":return tn}case M.Turbo:return(n,a)=>{const o=(a-e)/i;ln(n,o)}}}function ki(s,e){const t=e>>>0;s.r=u(((t&4278190080)>>>24)/255),s.g=u(((t&16711680)>>>16)/255),s.b=u(((t&65280)>>>8)/255),s.a=1}function Ji(s,e){const t=e>>>0;s.r=u(((t&4278190080)>>>24)/255),s.g=u(((t&16711680)>>>16)/255),s.b=u(((t&65280)>>>8)/255),s.a=((t&255)>>>0)/255}function Qi(s,e){const t=e>>>0;s.r=u(((t&65280)>>>8)/255),s.g=u(((t&16711680)>>>16)/255),s.b=u(((t&4278190080)>>>24)/255),s.a=1}function qi(s,e){const t=e>>>0;s.r=u(((t&65280)>>>8)/255),s.g=u(((t&16711680)>>>16)/255),s.b=u(((t&4278190080)>>>24)/255),s.a=((t&255)>>>0)/255}function en(s,e){const t=e>>>0;s.r=u(((t&255)>>>0)/255),s.g=u(((t&65280)>>>8)/255),s.b=u(((t&16711680)>>>16)/255),s.a=1}function tn(s,e){const t=e>>>0;s.r=u(((t&255)>>>0)/255),s.g=u(((t&65280)>>>8)/255),s.b=u(((t&16711680)>>>16)/255),s.a=((t&4278190080)>>>24)/255}function sn(s,e){const t=(1-Ce(e,0,1))*5+1,i=Math.floor(t);let n=t%1;i%2<1&&(n=1-n);const a=u(1-n);i<=1?(s.r=a,s.g=0,s.b=1):i===2?(s.r=0,s.g=a,s.b=1):i===3?(s.r=0,s.g=1,s.b=a):i===4?(s.r=a,s.g=1,s.b=0):(s.r=1,s.g=a,s.b=0),s.a=1}const nn=new r.Ltg(.13572138,4.6153926,-42.66032258,132.13108234),an=new r.Ltg(.09140261,2.19418839,4.84296658,-14.18503333),rn=new r.Ltg(.1066733,12.64194608,-60.58204836,110.36276771),on=new r.FM8(-152.94239396,59.28637943),cn=new r.FM8(4.27729857,2.82956604),hn=new r.FM8(-89.90310912,27.34824973),le=new r.Ltg,Oe=new r.FM8;function dn(s,e){const t=Ce(e,0,1)*.99+.01;le.set(1,t,t*t,t*t*t),Oe.set(le.z,le.w),Oe.multiplyScalar(le.z),s.r=u(Ce(le.dot(nn)+Oe.dot(on),0,1)),s.g=u(Ce(le.dot(an)+Oe.dot(cn),0,1)),s.b=u(Ce(le.dot(rn)+Oe.dot(hn),0,1)),s.a=1}let ie;const et=65535;function ln(s,e){if(!ie){ie=new Float32Array(et*3);const i={r:0,g:0,b:0,a:0};for(let n=0;n<et;n++){dn(i,n/(et-1));const a=n*3;ie[a+0]=i.r,ie[a+1]=i.g,ie[a+2]=i.b}}const t=Math.trunc(e*(et-1))*3;s.r=ie[t+0],s.g=ie[t+1],s.b=ie[t+2],s.a=1}function fn(s){return(e,t)=>e.getInt8(t+s)}function un(s){return(e,t)=>e.getUint8(t+s)}function pn(s){return(e,t)=>e.getInt16(t+s,!0)}function mn(s){return(e,t)=>e.getUint16(t+s,!0)}function gn(s){return(e,t)=>e.getInt32(t+s,!0)}function yn(s){return(e,t)=>e.getUint32(t+s,!0)}function vn(s){return(e,t)=>e.getFloat32(t+s,!0)}function wn(s){return(e,t)=>e.getFloat64(t+s,!0)}function tt(s,e){switch(s.datatype){case j.INT8:return s.offset+1<=e?fn(s.offset):void 0;case j.UINT8:return s.offset+1<=e?un(s.offset):void 0;case j.INT16:return s.offset+2<=e?pn(s.offset):void 0;case j.UINT16:return s.offset+2<=e?mn(s.offset):void 0;case j.INT32:return s.offset+4<=e?gn(s.offset):void 0;case j.UINT32:return s.offset+4<=e?yn(s.offset):void 0;case j.FLOAT32:return s.offset+4<=e?vn(s.offset):void 0;case j.FLOAT64:return s.offset+8<=e?wn(s.offset):void 0;default:return}}const Tn=1.5,Sn="circle",_s=M.Turbo,xn={r:1,g:1,b:1,a:1},_n={r:0,g:0,b:1,a:1},In={r:1,g:0,b:0,a:1},En="rgba",Mn=new Set(["rgb","rgba","bgr","bgra","abgr","color"]),Cn=new Set(["intensity","i"]),Ln="INVALID_POINT_CLOUD",ze={r:0,g:0,b:0,a:0};class Dn extends r.Tme{constructor(e){super();this.pointCloudsByTopic=new Map,this.renderer=e}dispose(){for(const e of this.pointCloudsByTopic.values()){Pn(e.userData.settings,this.renderer.materialCache);const t=e.userData.points;t.geometry.dispose(),t.userData.pickingMaterial.dispose()}this.children.length=0,this.pointCloudsByTopic.clear()}addPointCloud2Message(e,t){let i=this.pointCloudsByTopic.get(e);if(!i){i=new r.Tme,i.name=e,i.userData.topic=e,i.userData.settings={pointSize:Tn,pointShape:Sn,decayTime:0,colorMode:M.Flat,rgbByteOrder:En,flatColor:xn,minColor:_n,maxColor:In},Rn(i.userData.settings,t),i.userData.pointCloud=t,i.userData.pose=mt(),i.userData.srcTime=ye(t.header.stamp);const n=new Ts(Float32Array);n.name=`${e}:PointCloud2:geometry`,n.createAttribute("position",3),n.createAttribute("color",4),i.userData.geometry=n;const a=bn(i.userData.settings,this.renderer.materialCache),o=new r.woe(n,a);o.name=`${e}:PointCloud2:points`,o.userData.pickingMaterial=An(i.userData.settings),i.userData.points=o,i.add(i.userData.points),this.add(i),this.pointCloudsByTopic.set(e,i)}this._updatePointCloudRenderable(i,t)}startFrame(e){const t=this.renderer.renderFrameId,i=this.renderer.fixedFrameId;if(!(!t||!i))for(const n of this.pointCloudsByTopic.values()){const a=n.userData.srcTime,o=n.userData.pointCloud.header.frame_id;if(!He(n,this.renderer.transformTree,t,i,o,e,a)){const h=ke(t,i,o);this.renderer.layerErrors.addToTopic(n.userData.topic,Ke,h)}}}_updatePointCloudRenderable(e,t){e.userData.pointCloud=t,e.userData.srcTime=ye(t.header.stamp);const i=e.userData.settings,n=t.data,a=Math.trunc(n.length/t.point_step);if(t.is_bigendian){const x="PointCloud2 is_bigendian=true is not supported";k(this.renderer,e,x);return}else if(n.length%t.point_step!==0){const x=`PointCloud2 data length ${n.length} is not a multiple of point_step ${t.point_step}`;k(this.renderer,e,x);return}else if(t.fields.length===0){const x="PointCloud2 has no fields";k(this.renderer,e,x);return}else if(n.length<t.height*t.row_step){const x=`PointCloud2 data length ${n.length} is less than height ${t.height} * row_step ${t.row_step}`;k(this.renderer,e,x);return}else if(t.width*t.point_step>t.row_step){const x=`PointCloud2 width ${t.width} * point_step ${t.point_step} is greater than row_step ${t.row_step}`;k(this.renderer,e,x);return}let o,c,h,d;for(let x=0;x<t.fields.length;x++){const I=t.fields[x];if(I.name==="x"){if(o=tt(I,t.point_step),!o){const V=`PointCloud2 field "x" is invalid. type=${st(I.datatype)}, offset=${I.offset}, point_step=${t.point_step}`;k(this.renderer,e,V);return}}else if(I.name==="y"){if(c=tt(I,t.point_step),!c){const V=`PointCloud2 field "y" is invalid. type=${st(I.datatype)}, offset=${I.offset}, point_step=${t.point_step}`;k(this.renderer,e,V);return}}else if(I.name==="z"&&(h=tt(I,t.point_step),!h)){const V=`PointCloud2 field "z" is invalid. type=${st(I.datatype)}, offset=${I.offset}, point_step=${t.point_step}`;k(this.renderer,e,V);return}if(I.name===i.colorField&&(d=tt(I,t.point_step),!d)){const $=st(I.datatype),V=`PointCloud2 field "${I.name}" is invalid. type=${$}, offset=${I.offset}, point_step=${t.point_step}`;k(this.renderer,e,V);return}if(o&&c&&h&&d)break}if((o?1:0)+(c?1:0)+(h?1:0)<2){const x="PointCloud2 must contain at least two of x/y/z fields";k(this.renderer,e,x);return}d??(d=o??c??h??it),o??(o=it),c??(c=it),h??(h=it);const p=e.userData.geometry;p.resize(a);const l=p.getAttribute("position"),S=p.getAttribute("color"),m=new DataView(n.buffer,n.byteOffset,n.byteLength);let T=i.minValue??Number.POSITIVE_INFINITY,b=i.maxValue??Number.NEGATIVE_INFINITY;if(i.minValue==null||i.maxValue==null){for(let x=0;x<a;x++){const I=x*t.point_step,$=d(m,I);T=Math.min(T,$),b=Math.max(b,$)}T=i.minValue??T,b=i.maxValue??b}const G=Ki(i,T,b);for(let x=0;x<a;x++){const I=x*t.point_step,$=o(m,I),V=c(m,I),Gt=h(m,I);l.setXYZ(x,$,V,Gt);const w=d(m,I);G(ze,w),S.setXYZW(x,ze.r,ze.g,ze.b,ze.a)}l.needsUpdate=!0,S.needsUpdate=!0}}function bn(s,e){const t=Is(s),i={x:s.pointSize,y:s.pointSize};return e.acquire(J.id(i,t),()=>J.create(i,t),J.dispose)}function Pn(s,e){const t=Is(s),i={x:s.pointSize,y:s.pointSize};e.release(J.id(i,t))}function An(s){const e=8,t=Math.max(s.pointSize,e);return new r.jyz({vertexShader:`
      uniform float pointSize;
      void main() {
        gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
        gl_PointSize = pointSize;
      }
    `,fragmentShader:`
      uniform vec4 objectId;
      void main() {
        gl_FragColor = objectId;
      }
    `,side:r.ehD,uniforms:{pointSize:{value:t},objectId:{value:[NaN,NaN,NaN,NaN]}}})}function Is(s){switch(s.colorMode){case M.Flat:return s.flatColor.a<1;case M.Gradient:return s.minColor.a<1||s.maxColor.a<1;case M.Rainbow:case M.Turbo:case M.Rgb:return!1;case M.Rgba:return!0}}function Rn(s,e){for(const t of e.fields)if(Mn.has(t.name)){switch(s.colorField=t.name,t.name){case"rgb":s.colorMode=M.Rgb,s.rgbByteOrder="rgba";break;default:case"rgba":s.colorMode=M.Rgba,s.rgbByteOrder="rgba";break;case"bgr":s.colorMode=M.Rgb,s.rgbByteOrder="bgra";break;case"bgra":s.colorMode=M.Rgba,s.rgbByteOrder="bgra";break;case"abgr":s.colorMode=M.Rgba,s.rgbByteOrder="abgr";break}return}for(const t of e.fields)if(Cn.has(t.name)){s.colorField=t.name,s.colorMode=_s;return}if(e.fields.length>0){const t=e.fields[0];s.colorField=t.name,s.colorMode=_s;return}}function st(s){return j[s]??`${s}`}function k(s,e,t){s.layerErrors.addToTopic(e.userData.topic,Ln,t),e.userData.positionAttribute.resize(0),e.userData.colorAttribute.resize(0)}function it(){return 0}var Nn=y(63296);function On(s,e){return s<e?-1:s>e?1:0}function _t(s){const e=Math.trunc(Number(s/BigInt(1e9))),t=Number(s%BigInt(1e9));return e+t*1e-9}function zn(s){let e=Math.trunc(s),t=Math.round((s-e)*1e9);return e+=Math.trunc(t/1e9),t%=1e9,BigInt(e)*BigInt(1e9)+BigInt(t)}function Gn(s,e,t){const i=e-s,n=t-s;return _t(n)/_t(i)}function Fn(s,e,t){const i=e-s;return s+zn(t*_t(i))}const Es=4294967295n*BigInt(1e9),Ms=[0n,he.Identity()],Cs=[0n,he.Identity()],It=he.Identity(),Ls=[0n,It],ve=Qt();class ne{constructor(e,t,i){this.id=e,this.maxStorageTime=i,this._parent=t,this._transforms=new Nn.AVLTree(On)}parent(){return this._parent}root(){let e=this;for(;e._parent;)e=e._parent;return e}setParent(e){this._parent&&this._parent!==e&&this._transforms.clear(),this._parent=e}findAncestor(e){let t=this._parent;for(;t;){if(t.id===e)return t;t=t._parent}}addTransform(e,t){this._transforms.set(e,t);const n=this._transforms.maxKey()-this.maxStorageTime;for(;this._transforms.size>1&&this._transforms.minKey()<n;)this._transforms.shift()}findClosestTransforms(e,t,i,n){if(this._transforms.size===0)return!1;const a=this._transforms.findLessThanOrEqual(i);if(!a)return!1;if(this._transforms.size===1){const[p,l]=a;return i<=p+n?(e[0]=t[0]=p,e[1]=t[1]=l,!0):!1}const[o,c]=a;if(o===i)return e[0]=t[0]=o,e[1]=t[1]=c,!0;const h=this._transforms.findGreaterThan(i);if(!h){const[p,l]=this._transforms.maxEntry();return i<=p+n?(e[0]=t[0]=p,e[1]=t[1]=l,!0):!1}const[d,f]=h;return e[0]=o,e[1]=c,t[0]=d,t[1]=f,!0}applyLocal(e,t,i,n,a=Es){if(i===this)return Un(e,t),e;if(i.findAncestor(this.id))return ne.Apply(e,t,this,i,!1,n,a)?e:void 0;if(this.findAncestor(i.id))return ne.Apply(e,t,i,this,!0,n,a)?e:void 0;let o=i;for(;o;){const c=this.findAncestor(o.id);if(c)return ne.Apply(e,t,c,i,!1,n,a)&&ne.Apply(e,e,c,this,!0,n,a)?e:void 0;o=o._parent}}apply(e,t,i,n,a,o,c=Es){if(i.applyLocal(e,t,n,o,c)!=null)return this.applyLocal(e,e,i,a,c)}static Interpolate(e,t,i,n){const[a,o]=t,[c,h]=i;if(a===c){e[0]=c,e[1].copy(h);return}const d=Math.max(0,Math.min(1,Gn(a,c,n)));e[0]=Fn(a,c,d),he.Interpolate(e[1],o,h,d)}static GetTransformMatrix(e,t,i,n,a){N.yR(e);let o=i;for(;o!==t;){if(!o.findClosestTransforms(Ms,Cs,n,a))return!1;if(ne.Interpolate(Ls,Ms,Cs,n),N.Jp(e,Ls[1].matrix(),e),o._parent==null)throw new Error(`Frame "${t.id}" is not a parent of "${i.id}"`);o=o._parent}return!0}static Apply(e,t,i,n,a,o,c){return ne.GetTransformMatrix(ve,i,n,o,c)?(a&&N.U_(ve,ve),N.Jp(ve,ve,It.setPose(t).matrix()),It.setMatrix(ve).toPose(e),!0):!1}}function Un(s,e){const t=e.position,i=e.orientation;s.position.x=t.x,s.position.y=t.y,s.position.z=t.z,s.orientation.x=i.x,s.orientation.y=i.y,s.orientation.z=i.z,s.orientation.w=i.w}const Bn=10n*BigInt(1e9);class Et{constructor(e=Bn){this._frames=new Map,this._maxStorageTime=e}addTransform(e,t,i,n){const a=this.getOrCreateFrame(e),o=a.parent();(o==null||o.id!==t)&&a.setParent(this.getOrCreateFrame(t)),a.addTransform(i,n)}hasFrame(e){return this._frames.has(e)}frame(e){return this._frames.get(e)}getOrCreateFrame(e){let t=this._frames.get(e);return t||(t=new ne(e,void 0,this._maxStorageTime),this._frames.set(e,t)),t}frames(){return this._frames}apply(e,t,i,n,a,o,c,h){const d=this.frame(i),f=this.frame(a);if(!d||!f)return;const p=(n!=null?this.frame(n):d.root())??d.root();return d.apply(e,t,p,f,o,c,h)}static Clone(e){const t=new Et(e._maxStorageTime);return t._frames=e._frames,t}}var jn="../../packages/studio-base/src/panels/ThreeDeeRender/Renderer.ts";const we=Ge.Z.getLogger(jn),Ds=!1,Hn=new r.Ilk(15527148),$n=new r.Ilk(1184279),Vn=new r.Ilk(0).convertSRGBToLinear(),Wn=new r.Ilk(16777215).convertSRGBToLinear(),Mt=0,Ct=1,Yn=60n*BigInt(1e9),Zn=new r.Pa4(1,0,0),Xn=Math.PI/2,nt=new r.Pa4,bs=new r.FM8,Kn=new r.$V,kn=new r.USm;class Jn extends Vt(){constructor(e){super();if(this.maxLod=L.High,this.materialCache=new ei,this.layerErrors=new Ks,this.renderables=new Map,this.transformTree=new Et(Yn),this.frameAxes=new E(this),this.occupancyGrids=new q(this),this.pointClouds=new Dn(this),this.markers=new Fi(this),this.animationFrame=()=>{this.currentTime!=null&&this.frameHandler(this.currentTime)},this.frameHandler=d=>{this.emit("startFrame",d,this),this.fixedFrameId="map",this.renderFrameId="base_link",this.materialCache.update(this.input.canvasSize),this.frameAxes.startFrame(d),this.occupancyGrids.startFrame(d),this.pointClouds.startFrame(d),this.markers.startFrame(d),this.gl.clear(),this.camera.layers.set(Mt),this.selectionBackdrop.visible=this.selectedObject!=null,this.gl.render(this.scene,this.camera),this.selectedObject&&(this.gl.clearDepth(),this.camera.layers.set(Ct),this.selectionBackdrop.visible=!1,this.gl.render(this.scene,this.camera)),this.emit("endFrame",d,this),this.gl.info.reset()},this.resizeHandler=d=>{this.gl.setPixelRatio(window.devicePixelRatio),this.gl.setSize(d.width,d.height);const f=this.gl.getDrawingBufferSize(bs);this.camera.aspect=f.width/f.height,this.camera.updateProjectionMatrix(),we.debug(`Resized renderer to ${f.width}x${f.height}`),this.animationFrame()},this.clickHandler=d=>{this.selectedObject&&(qn(this.selectedObject),this.selectedObject=void 0),this.animationFrame();const f=this.picker.pick(d.x,d.y);if(f<0){we.debug("Background selected"),this.emit("renderableSelected",void 0,this);return}let l=this.scene.getObjectById(f);for(;l&&l.name==="";)l=l.parent??void 0;if(this.selectedObject=l,!l){we.warn(`No renderable found for objectId ${f}`),this.emit("renderableSelected",void 0,this);return}Qn(l),this.emit("renderableSelected",l,this),we.debug(`Selected object ${l.name}`),Ds||this.animationFrame()},r.Tme.DefaultUp=new r.Pa4(0,0,1),this.canvas=e,this.gl=new r.CP7({canvas:e,alpha:!0,antialias:!0}),!this.gl.capabilities.isWebGL2)throw new Error("WebGL2 is not supported");this.gl.outputEncoding=r.knz,this.gl.toneMapping=r.uL9,this.gl.autoClear=!1,this.gl.info.autoReset=!1,this.gl.shadowMap.enabled=!1,this.gl.shadowMap.type=r.dwk,this.gl.setPixelRatio(window.devicePixelRatio);let t=e.width,i=e.height;e.parentElement&&(t=e.parentElement.clientWidth,i=e.parentElement.clientHeight,this.gl.setSize(t,i)),this.modelCache=new ii({ignoreColladaUpAxis:!0}),this.scene=new r.xsS,this.scene.add(this.frameAxes),this.scene.add(this.occupancyGrids),this.scene.add(this.pointClouds),this.scene.add(this.markers),this.dirLight=new r.Ox3,this.dirLight.position.set(1,1,1),this.dirLight.castShadow=!0,this.dirLight.layers.enableAll(),this.dirLight.shadow.mapSize.width=2048,this.dirLight.shadow.mapSize.height=2048,this.dirLight.shadow.camera.near=.5,this.dirLight.shadow.camera.far=500,this.dirLight.shadow.bias=-1e-5,this.hemiLight=new r.vmT(16777215,16777215,.5),this.hemiLight.layers.enableAll(),this.scene.add(this.dirLight),this.scene.add(this.hemiLight),this.input=new Ws(e),this.input.on("resize",d=>this.resizeHandler(d)),this.input.on("click",d=>this.clickHandler(d));const n=79,a=.01,o=1e4;this.camera=new r.cPb(n,t/i,a,o),this.camera.up.set(0,0,1),this.camera.position.set(1,-3,1),this.camera.lookAt(0,0,0),this.picker=new ai(this.gl,this.scene,this.camera,{debug:Ds}),this.selectionBackdrop=new ae,this.selectionBackdrop.visible=!1,this.scene.add(this.selectionBackdrop);const c=oi(this.maxLod,this.gl.capabilities),h=this.gl.getDrawingBufferSize(bs);we.debug(`Initialized ${h.width}x${h.height} renderer (${c}x MSAA)`),this.animationFrame()}dispose(){this.removeAllListeners(),this.picker.dispose(),this.input.dispose(),this.frameAxes.dispose(),this.occupancyGrids.dispose(),this.pointClouds.dispose(),this.markers.dispose(),this.gl.dispose()}setColorScheme(e){we.debug(`Setting color scheme to "${e}"`),this.colorScheme=e,e==="dark"?(this.gl.setClearColor($n),this.materialCache.outlineMaterial.color.set(Wn),this.materialCache.outlineMaterial.needsUpdate=!0):(this.gl.setClearColor(Hn),this.materialCache.outlineMaterial.color.set(Vn),this.materialCache.outlineMaterial.needsUpdate=!0)}addTransformMessage(e){this.frameAxes.addTransformMessage(e)}addOccupancyGridMessage(e,t){this.occupancyGrids.addOccupancyGridMessage(e,t)}addPointCloud2Message(e,t){this.pointClouds.addPointCloud2Message(e,t)}addMarkerMessage(e,t){this.markers.addMarkerMessage(e,t)}markerWorldPosition(e){const t=this.renderables.get(e);if(!!t)return nt.set(0,0,0),nt.applyMatrix4(t.matrixWorld),nt}setCameraState(e){this.camera.position.setFromSpherical(Kn.set(e.distance,e.phi,-e.thetaOffset)).applyAxisAngle(Zn,Xn),this.camera.position.add(nt.set(e.targetOffset[0],e.targetOffset[1],e.targetOffset[2])),this.camera.quaternion.setFromEuler(kn.set(e.phi,0,-e.thetaOffset,"ZYX")),this.camera.updateProjectionMatrix()}}function Qn(s){s.layers.set(Ct),s.traverse(e=>{e.layers.set(Ct)})}function qn(s){s.layers.set(Mt),s.traverse(e=>{e.layers.set(Mt)})}let U,Lt,Dt,bt,Pt,At=0,Rt=0,Nt=0,Ot=0;function ea(s){At=Math.max(At,s.gl.info.render.calls),Rt=Math.max(Rt,s.gl.info.render.triangles),Nt=Math.max(Nt,s.gl.info.memory.textures),Ot=Math.max(Ot,s.gl.info.memory.geometries),Lt?.update(s.gl.info.render.calls,At),Dt?.update(s.gl.info.render.triangles,Rt),bt?.update(s.gl.info.memory.textures,Nt),Pt?.update(s.gl.info.memory.geometries,Ot),U?.update()}function ta(){const[s,e]=(0,g.useState)(null),t=Fe();return xe("endFrame",()=>t&&ea(t)),(0,g.useEffect)(()=>{if(!!s)return U=new sa,U.dom.style.position="relative",U.dom.style.zIndex="auto",Lt=new fe("draws","red","black"),Dt=new fe("tris","cyan","black"),bt=new fe("textures","yellow","black"),Pt=new fe("geometries","green","black"),U.addPanel(Lt),U.addPanel(Dt),U.addPanel(bt),U.addPanel(Pt),s.appendChild(U.dom),U.showPanel(0),()=>{if(U)try{s.removeChild(U.dom)}catch{}}},[s]),(0,C.jsx)("div",{ref:e})}class sa{constructor(){this.mode=0,this.frames=0,this.begin=()=>{this.beginTime=performance.now()},this.end=()=>{this.frames++;const e=performance.now();if(this.msPanel.update(e-this.beginTime,1e3/30),e>=this.prevTime+1e3){this.fpsPanel.update(this.frames*1e3/(e-this.prevTime),100),this.prevTime=e,this.frames=0;const t=performance.memory;this.memPanel.update(t.usedJSHeapSize/1048576,t.jsHeapSizeLimit/1048576)}return e},this.update=()=>{this.beginTime=this.end()},this.container=document.createElement("div"),this.container.style.cssText="position:fixed;top:0;left:0;cursor:pointer;opacity:0.9;z-index:10000",this.container.addEventListener("click",e=>{e.preventDefault(),this.showPanel(++this.mode%this.container.children.length)},!1),this.dom=this.container,this.beginTime=performance.now(),this.prevTime=this.beginTime,this.msPanel=this.addPanel(new fe("MS","#0f0","#020")),this.fpsPanel=this.addPanel(new fe("FPS","#0ff","#002")),this.memPanel=this.addPanel(new fe("MB","#f08","#201")),this.showPanel(0)}addPanel(e){return this.container.appendChild(e.dom),e}showPanel(e){for(let t=0;t<this.container.children.length;t++){const i=this.container.children[t];i.style.display=t===e?"block":"none"}this.mode=e}}class fe{constructor(e,t,i){this.min=1/0,this.max=0,this.name=e,this.fg=t,this.bg=i;const n=Math.round(window.devicePixelRatio),a=80*n,o=48*n,c=3*n,h=2*n,d=3*n,f=15*n,p=74*n,l=30*n,S=document.createElement("canvas");S.width=a,S.height=o,S.style.cssText="width:80px;height:48px";const m=S.getContext("2d");m.font=`bold ${9*n}px Helvetica,Arial,sans-serif`,m.textBaseline="top",m.fillStyle=i,m.fillRect(0,0,a,o),m.fillStyle=t,m.fillText(e,c,h),m.fillRect(d,f,p,l),m.fillStyle=i,m.globalAlpha=.9,m.fillRect(d,f,p,l),this.dom=S,this.context=m}update(e,t){const i=Math.round(window.devicePixelRatio),n=80*i,a=3*i,o=2*i,c=3*i,h=15*i,d=74*i,f=30*i;this.min=Math.min(this.min,e),this.max=Math.max(this.max,e),this.context.fillStyle=this.bg,this.context.globalAlpha=1,this.context.fillRect(0,0,n,h),this.context.fillStyle=this.fg,this.context.fillText(`${Math.round(e)} ${this.name} (${Math.round(this.min)}-${Math.round(this.max)})`,a,o),this.context.drawImage(this.dom,c+i,h,d-i,f,c,h,d-i,f),this.context.fillRect(c+d-i,h,i,f),this.context.fillStyle=this.bg,this.context.globalAlpha=.9,this.context.fillRect(c+d-i,h,i,Math.round((1-e/t)*f))}}var ia="../../packages/studio-base/src/panels/ThreeDeeRender/ThreeDeeRender.tsx";const na=!0,aa=!1,ue=new Set;Te(ue,je),Te(ue,Ae),Te(ue,lt),Te(ue,ft),Te(ue,ut),Te(ue,pt);const ra=Ge.Z.getLogger(ia),oa=Ut.Z`
  position: relative;
  color: #27272b;
  background-color: #ececec99;
`,ca=Ut.Z`
  position: relative;
  color: #e1e1e4;
  background-color: #181818cc;
`;function ha(s){const e=s.colorScheme,[t,i]=(0,g.useState)(void 0),[n,a]=(0,g.useState)(new Map),o=(0,g.useRef)(null),c=Fe();xe("renderableSelected",l=>i(l)),xe("showLabel",(l,S)=>{n.get(l)!==S&&a(new Map(n.set(l,S)))}),xe("removeLabel",l=>{!n.has(l)||(n.delete(l),a(new Map(n)))}),xe("endFrame",(l,S)=>{if(o.current)for(const m of n.keys()){const T=document.getElementById(`label-${m}`);if(T){const b=S.markerWorldPosition(m);b&&$t(T.style,b,S.input.canvasSize,S.camera)}}});const h=(0,g.useMemo)(()=>{const l=[];if(!c)return l;const S={left:"",top:"",transform:""},m=e==="dark"?ca:oa;for(const[T,b]of n){const G=c.markerWorldPosition(T);G&&($t(S,G,c.input.canvasSize,c.camera),l.push((0,C.jsx)("div",{id:`label-${T}`,className:m.name,style:S,children:b.text},T)))}return l},[c,n,e]),d=(0,C.jsx)("div",{id:"labels",ref:o,style:{position:"absolute",top:0},children:h}),f=na?(0,C.jsx)("div",{id:"stats",style:{position:"absolute",top:0},children:(0,C.jsx)(ta,{})}):void 0,p=aa?(0,C.jsx)("div",{id:"debug",style:{position:"absolute",top:60},children:(0,C.jsx)(Us,{})}):void 0;return(0,C.jsxs)(g.Fragment,{children:[d,f,p]})}function da({context:s}){const[e,t]=(0,g.useState)(null),[i,n]=(0,g.useState)(null);(0,g.useEffect)(()=>n(e?new Jn(e):null),[e]);const[a,o]=(0,g.useState)(),[c,h]=(0,g.useState)(),[d,f]=(0,g.useState)(),[p,l]=(0,g.useState)(),[S,m]=(0,g.useState)();(0,Gs.Z)(()=>{i?.dispose()}),(0,g.useLayoutEffect)(()=>{s.onRender=(w,P)=>{if(w.currentTime&&l((0,zs.toNanoSec)(w.currentTime)),m(P),o(w.colorScheme),h(w.topics),w.currentFrame)for(const z of w.currentFrame){const Se=z.message;"toJSON"in Se&&(z.message=Se.toJSON())}f(w.currentFrame)},s.watch("currentTime"),s.watch("colorScheme"),s.watch("topics"),s.watch("currentFrame")},[s]);const T=(0,g.useMemo)(()=>{const w=new Map;if(!c)return w;for(const P of c)w.set(P.name,P.datatype);return w},[c]),b=(0,g.useMemo)(()=>{const w=[];if(!!c){for(const P of c)(Ae.has(P.datatype)||je.has(P.datatype))&&w.push(P.name),ue.has(P.datatype)&&w.push(P.name);return w}},[c]);(0,g.useEffect)(()=>{!b||(ra.debug(`Subscribing to [${b.join(", ")}]`),s.subscribe(b))},[s,b]),(0,g.useEffect)(()=>{i&&p!=null&&(i.currentTime=p)},[p,i]),(0,g.useEffect)(()=>{a&&i&&i.setColorScheme(a)},[a,i]);const[G,x]=(0,g.useState)(()=>at.dR),[I]=(0,g.useState)(()=>new at.Nk(x,G));(0,g.useEffect)(()=>{if(!!i){if(i.setCameraState(G),!d){i.animationFrame();return}for(const w of d){const P=T.get(w.topic);if(!!P){if(Ae.has(P)){const z=w.message;for(const Se of z.transforms)i.addTransformMessage(Se)}else if(je.has(P)){const z=w.message;i.addTransformMessage(z)}else if(ft.has(P)){const z=w.message;for(const Se of z.markers)i.addMarkerMessage(w.topic,Se)}else if(lt.has(P)){const z=w.message;i.addMarkerMessage(w.topic,z)}else if(ut.has(P)){const z=w.message;i.addOccupancyGridMessage(w.topic,z)}else if(pt.has(P)){const z=w.message;i.addPointCloud2Message(w.topic,z)}}}i.animationFrame()}},[G,d,i,T]),(0,g.useEffect)(()=>{S?.()},[S]);const{ref:$,width:V,height:Gt}=(0,Os.NB)({refreshRate:0,refreshMode:"debounce"});return(0,C.jsxs)("div",{style:{width:"100%",height:"100%",display:"flex"},ref:$,children:[(0,C.jsxs)(at.Rx,{cameraStore:I,shiftKeys:!0,children:[(0,C.jsx)("div",{style:{width:V,height:Gt}}),(0,C.jsx)("canvas",{ref:t,style:{position:"absolute",top:0,left:0}})]}),(0,C.jsx)(Bt.Provider,{value:i,children:(0,C.jsx)(ha,{colorScheme:a})})]})}function Te(s,e){for(const t of e)s.add(t)}var la=y(27431);function fa(s){Ps.render((0,C.jsx)(g.StrictMode,{children:(0,C.jsx)(Ns.Z,{isDark:!0,children:(0,C.jsx)(da,{context:s})})}),s.panelElement)}function zt(s){return(0,C.jsx)(Rs.Z,{config:s.config,saveConfig:s.saveConfig,help:la,initPanel:fa})}zt.panelType="3D",zt.defaultConfig={};const ua=(0,As.Z)(zt)},47626:()=>{}}]);

//# sourceMappingURL=323.2124e7b031ab6b7e6542.js.map