"use strict";(self.webpackChunk=self.webpackChunk||[]).push([[6008],{93981:(u,R,t)=>{t.r(R),t.d(R,{default:()=>it});var e=t(52322),se=t(26948),T=t(82598),ie=t(50764),ae=t(50402),le=t(64798),Q=t(79168),A=t(33927),G=t(83422),p=t(2784),L=t(22915),re=t(9571),W=t(43017),z=t(51368),F=t(37825),de=t(92168),ce=t(88058),ue=t(92750),U=t(88099),pe=t(87023),H=t(31748),Me=t(55601),ge=t(52643),Ae=t(45004),me=t(57422),ye=t(24374),he=t(76635),je=t(56924);const xe={Hint:(0,e.jsx)(me.Z,{}),Info:(0,e.jsx)(ye.Z,{}),Warning:(0,e.jsx)(Ae.Z,{}),Error:(0,e.jsx)(ge.Z,{})},Ie=({diagnostics:n})=>{const s=(0,T.F)(),l={Hint:s.palette.yellowLight,Info:s.palette.blueLight,Warning:s.semanticColors.warningBackground,Error:s.semanticColors.errorBackground};return n.length>0?(0,e.jsx)("ul",{style:{listStyle:"none",padding:0},children:n.map(({severity:a,message:o,source:i,startColumn:r,startLineNumber:g},h)=>{const x=(0,he.invert)(je.H_)[a]??"Error",C=g!=null&&r!=null?`[${g+1},${r+1}]`:void 0;return(0,e.jsxs)("li",{children:[(0,e.jsx)(z.Z,{tooltip:"Severity",size:"small",style:{color:l[x]},active:!0,children:xe[x]}),(0,e.jsx)("span",{style:{padding:"5px"},children:o}),(0,e.jsxs)("span",{style:{color:s.palette.neutralLight},children:[i," ",C]})]},`${o}_${h}`)})}):(0,e.jsx)("p",{children:"No problems to display."})};var De=t(30650),J=t(11701),D=t(77075);const fe=L.ZP.li.withConfig({displayName:"SListItem__src_panels_NodePlayground_BottomBar_LogsSection_tsx",componentId:"-o8j06f"})`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  cursor: default;

  :hover {
    background-color: ${D.O9.DARK4};
  }
`,Ce=({nodeId:n,logs:s,clearLogs:l})=>{const a=(0,J.lk)(),o={string:a.base0B,number:a.base09,boolean:a.base09,object:a.base08,undefined:a.base08};return s.length===0?(0,e.jsxs)(e.Fragment,{children:[(0,e.jsx)("p",{children:"No logs to display."}),(0,e.jsxs)("p",{children:["Invoke ",(0,e.jsx)("code",{children:"log(someValue)"})," in your Foxglove Studio node code to see data printed here."]})]}):(0,e.jsxs)(e.Fragment,{children:[(0,e.jsx)(F.mN,{"data-test":"np-logs-clear",style:{padding:"3px 5px",position:"absolute",right:5,top:5},onClick:()=>{n!=null&&l(n)},children:"clear logs"}),(0,e.jsx)("ul",{children:s.map(({source:i,value:r},g)=>{const h=r!=null&&typeof r=="object";return(0,e.jsxs)(fe,{style:{padding:h?"0px 3px":"6px 3px 3px"},children:[h?(0,e.jsx)(De.ZP,{hideRoot:!0,data:r,invertTheme:!1,theme:a}):(0,e.jsx)("span",{style:{color:o[typeof r]??D.O9.LIGHT},children:r==null||r===!1?String(r):r}),(0,e.jsx)("div",{style:{color:D.O9.DARK9,textDecoration:"underline"},children:i})]},`${g}${i}`)})})]})},Ne=L.ZP.div.withConfig({displayName:"SHeaderItem__src_panels_NodePlayground_BottomBar_index_tsx",componentId:"-1fewvr0"})`
  cursor: pointer;
  padding: 4px;
  text-transform: uppercase;
`;function K({isOpen:n,numItems:s,text:l}){const a=(0,T.F)();return(0,e.jsxs)(Ne,{style:{color:s>0?a.semanticColors.errorBackground:"inherit",borderBottom:n?`1px solid ${D.O9.DARK6}`:"none",paddingBottom:n?2:0},children:[l," ",s>0?s:""]})}const ve=({nodeId:n,isSaved:s,save:l,diagnostics:a,logs:o})=>{const[i,r]=(0,p.useState)("closed"),[g,h]=(0,p.useState)(!0),x=(0,T.F)(),{clearUserNodeLogs:C}=(0,H.BQ)(),v=(0,p.useRef)(null);return(0,p.useEffect)(()=>{g&&v.current&&(v.current.scrollTop=v.current.scrollHeight)},[g,o]),(0,e.jsxs)(A.Z,{flex:"auto",bgcolor:x.palette.neutralLighterAlt,position:"relative",children:[(0,e.jsxs)(A.Z,{direction:"row",flex:"auto",alignItems:"flex-start",padding:.625,bottom:0,children:[(0,e.jsx)(A.Z,{direction:"row",alignItems:"center",justifyContent:"center",color:D.O9.DARK9,"data-test":"np-errors",onClick:()=>{r(i!=="diagnostics"?"diagnostics":"closed")},children:(0,e.jsx)(K,{text:"Problems",numItems:a.length,isOpen:i==="diagnostics"})}),(0,e.jsx)(A.Z,{direction:"row",alignItems:"center",justifyContent:"center",color:D.O9.DARK9,"data-test":"np-logs",onClick:()=>{r(i!=="logs"?"logs":"closed")},children:(0,e.jsx)(K,{text:"Logs",numItems:o.length,isOpen:i==="logs"})}),(0,e.jsx)(W.Z,{style:{padding:"2px 4px"},primary:!s,disabled:s,tooltip:"ctrl/cmd + s",onClick:()=>{n!=null&&(l(),C(n))},children:s?"saved":"save"})]}),(0,e.jsxs)(G.Z,{ref:v,onScroll:({currentTarget:N})=>{const j=N.scrollHeight-N.scrollTop>N.clientHeight;j&&g?h(!1):!j&&!g&&h(!0)},sx:{overflowY:i!=="closed"?"scroll":"auto",height:i!=="closed"?150:0,color:D.O9.DARK9},children:[i==="diagnostics"&&(0,e.jsx)(Ie,{diagnostics:a}),i==="logs"&&(0,e.jsx)(Ce,{nodeId:n,logs:o,clearLogs:C})]})]})};var we=t(92679),Te=t(28614),Le=t(50334),ze=t(63077),Se=t(44419),k=t(17764),Ze=t(70943),Oe=t(45434),b=t(11259),S=t(12354),X=t(66198),Z=t(14117),O=t(82056),ke=t(65154),be=t(68434),Ee=t(90082),$=t(95971),Pe=t(76827),Ye=t(62728),Be=t(79156),Ve=t(62434),Re=t(32470);const Qe=[{name:"Skeleton",description:"An empty node script",template:Re},{name:"Markers",description:"A node that publishes one or more markers",template:Be},{name:"Multiple Inputs",description:"A node that receives inputs on multiple topics",template:Ve},{name:"GPS Location",description:"A node that publishes foxglove.LocationFix",template:Ye}],E=(0,k.ZP)(Ze.Z)(({theme:n})=>({minWidth:"auto",padding:n.spacing(1,1.125),"&.Mui-selected":{backgroundColor:n.palette.grey[100]}})),Ge=(0,k.ZP)(Oe.Z)({".MuiTabs-indicator":{display:"none"}}),We=(0,k.ZP)("div")(({theme:n})=>({backgroundColor:n.palette.grey[100],width:350,overflow:"auto"})),Fe=({nodes:n,selectNode:s,deleteNode:l,collapse:a,selectedNodeId:o})=>(0,e.jsxs)(A.Z,{flex:"auto",children:[(0,e.jsx)(P,{title:"Nodes",collapse:a}),(0,e.jsx)(b.Z,{dense:!0,children:Object.keys(n).map(i=>(0,e.jsx)(S.ZP,{disablePadding:!0,selected:o===i,secondaryAction:(0,e.jsx)(X.Z,{size:"small",onClick:()=>l(i),edge:"end","aria-label":"delete",title:"Delete",color:"error",children:(0,e.jsx)(Le.Z,{fontSize:"small"})}),children:(0,e.jsx)(Z.Z,{onClick:()=>s(i),children:(0,e.jsx)(O.Z,{primary:n[i]?.name,primaryTypographyProps:{variant:"body1"}})})},i))})]}),{utilityFiles:Ue}=(0,Pe.v)(),P=({title:n,subheader:s,collapse:l})=>(0,e.jsx)(ke.Z,{title:n,titleTypographyProps:{variant:"h5",gutterBottom:!0},subheader:s,subheaderTypographyProps:{variant:"body2",color:"text.secondary"},action:(0,e.jsx)(X.Z,{size:"small",onClick:l,title:"Collapse",children:(0,e.jsx)(we.Z,{})})}),He=({userNodes:n,selectNode:s,deleteNode:l,selectedNodeId:a,explorer:o,updateExplorer:i,setScriptOverride:r,script:g,addNewNode:h})=>{const x=o==="nodes",C=o==="utils",v=o==="templates",N=(0,p.useCallback)(d=>{const c=$.Uri.parse(`file://${d}`),I=$.editor.getModel(c);!I||r({filePath:I.uri.path,code:I.getValue(),readOnly:!0,selection:void 0},2)},[r]),j=(0,p.useMemo)(()=>{switch(o){case void 0:return!1;case"nodes":return"nodes";case"templates":return"templates";case"utils":return"utils"}return!1},[o]),B=(0,p.useMemo)(()=>({nodes:(0,e.jsx)(Fe,{nodes:n,selectNode:s,deleteNode:l,collapse:()=>i(void 0),selectedNodeId:a}),utils:(0,e.jsxs)(A.Z,{flex:"auto",position:"relative",children:[(0,e.jsx)(P,{collapse:()=>i(void 0),title:"Utilities",subheader:(0,e.jsxs)(be.Z,{variant:"body2",color:"text.secondary",component:"div",children:["You can import any of these modules into your node using the following syntax:"," ",(0,e.jsx)("pre",{children:'import { ... } from "./pointClouds.ts".'})]})}),(0,e.jsxs)(b.Z,{dense:!0,children:[Ue.map(({fileName:d,filePath:c})=>(0,e.jsx)(S.ZP,{disablePadding:!0,onClick:N.bind(void 0,c),selected:g?c===g.filePath:!1,children:(0,e.jsx)(Z.Z,{children:(0,e.jsx)(O.Z,{primary:d,primaryTypographyProps:{variant:"body1"}})})},c)),(0,e.jsx)(S.ZP,{disablePadding:!0,onClick:N.bind(void 0,"/studio_node/generatedTypes.ts"),selected:g?g.filePath==="/studio_node/generatedTypes.ts":!1,children:(0,e.jsx)(Z.Z,{children:(0,e.jsx)(O.Z,{primary:"generatedTypes.ts",primaryTypographyProps:{variant:"body1"}})})})]})]}),templates:(0,e.jsxs)(A.Z,{flex:"auto",children:[(0,e.jsx)(P,{title:"Templates",subheader:"Create nodes from these templates, click a template to create a new node.",collapse:()=>i(void 0)}),(0,e.jsx)(b.Z,{dense:!0,children:Qe.map(({name:d,description:c,template:I})=>(0,e.jsx)(S.ZP,{disablePadding:!0,onClick:()=>h(I),children:(0,e.jsx)(Z.Z,{children:(0,e.jsx)(O.Z,{primary:d,primaryTypographyProps:{variant:"body1"},secondary:c})})},d))})]})}),[h,l,N,g,s,a,i,n]);return(0,e.jsx)(Ee.Z,{children:(0,e.jsxs)(A.Z,{direction:"row",height:"100%",children:[(0,e.jsxs)(Ge,{orientation:"vertical",value:j,children:[(0,e.jsx)(E,{disableRipple:!0,value:"nodes",title:"Nodes",icon:(0,e.jsx)(Se.Z,{fontSize:"large"}),"data-test":"node-explorer",onClick:()=>i(x?void 0:"nodes")}),(0,e.jsx)(E,{disableRipple:!0,value:"utils",title:"Utilities",icon:(0,e.jsx)(Te.Z,{fontSize:"large"}),"data-test":"utils-explorer",onClick:()=>i(C?void 0:"utils")}),(0,e.jsx)(E,{disableRipple:!0,value:"templates",title:"Templates",icon:(0,e.jsx)(ze.Z,{fontSize:"large"}),"data-test":"templates-explorer",onClick:()=>i(v?void 0:"templates")})]}),o!=null&&(0,e.jsx)(We,{children:B[o]})]})})};function Y(){return Y=Object.assign||function(n){for(var s=1;s<arguments.length;s++){var l=arguments[s];for(var a in l)Object.prototype.hasOwnProperty.call(l,a)&&(n[a]=l[a])}return n},Y.apply(this,arguments)}const Je=({styles:n={},...s})=>p.createElement("svg",Y({width:"138",height:"107",viewBox:"0 0 138 107",fill:"none",xmlns:"http://www.w3.org/2000/svg"},s),p.createElement("path",{d:"M47.5 19L44 21l3.5 1.5M53.5 22.5L57 21l-3.5-2",stroke:"#C8C8C7",strokeLinecap:"round",strokeLinejoin:"round"}),p.createElement("path",{d:"M47 28l5-10.5",stroke:"#C8C8C7",strokeLinecap:"round"}),p.createElement("path",{d:"M0 106.5h39m9.5 0H79m5.5 0H109m11.5 0H136M29.076 47.5L47 27.744 64.924 47.5H29.076z",stroke:"#C8C8C7"}),p.createElement("path",{d:"M89.5 2.5l-3.5 2L89.5 6M95.5 6L99 4.5l-3.5-2",stroke:"#C8C8C7",strokeLinecap:"round",strokeLinejoin:"round"}),p.createElement("path",{d:"M89 11.5L94 1",stroke:"#C8C8C7",strokeLinecap:"round"}),p.createElement("path",{d:"M71.076 31L89 11.244 106.924 31H71.076zM34.5 79.238h25V89.5h-25V79.238zm25-1h-25V47.5h25v30.738zM76.5 37.073h25V71.5h-25V37.073zm25-1h-25V31h25v5.073z",stroke:"#C8C8C7"}),p.createElement("circle",{cx:"47",cy:"61",r:"4.5",stroke:"#C8C8C7"}),p.createElement("circle",{cx:"89",cy:"89",r:"5.5",stroke:"#C8C8C7"}),p.createElement("path",{d:"M113 71.5v-11H68m45 11V85c0 5 9.092 12.56 19 10.5l4.5 11H120m-7-35h-11.5M68 60.5h-8.5v11H68m0-11v11m0 0h8.5m43 35h.5m-18.5-35H89m12.5 0V89m0 17.5V89m-25-17.5V89m0-17.5H89m-12.5 35V89m0 0h7M89 71.5v12m5.5 5.5h7m0 0c1.6 9.6 13 15.667 18.5 17.5m-31-12v12M49.075 34.928l4.85 5.144M59.5 89.5v17M34.5 89.5v17M.5 93l34-28.5M1.5 106.5l33-27.5M14 106.5l20.5-17M6 88.5L5.5 103M14 82l-.5 14.5M22 75l-.5 14.5m8.5-21L29.5 83",stroke:"#C8C8C7"}));var q=t(9119),f=t(2784);const Ke=f.lazy(async()=>await t.e(6716).then(t.bind(t,96716))),Xe=`// The ./types module provides helper types for your Input events and messages.
import { Input, Message } from "./types";

// Your node can output well-known message types, any of your custom message types, or
// complete custom message types.
//
// Use \`Message\` to access your data source types or well-known types:
// type Twist = Message<"geometry_msgs/Twist">;
//
// Conventionally, it's common to make a _type alias_ for your node's output type
// and use that type name as the return type for your node function.
// Here we've called the type \`Output\` but you can pick any type name.
type Output = {
  hello: string;
};

// These are the topics your node "subscribes" to. Studio will invoke your node function
// when any message is received on one of these topics.
export const inputs = ["/input/topic"];

// Any output your node produces is "published" to this topic. Published messages are only visible within Studio, not to your original data source.
export const output = "/studio_node/output_topic";

// This function is called with messages from your input topics.
// The first argument is an event with the topic, receive time, and message.
// Use the \`Input<...>\` helper to get the correct event type for your input topic messages.
export default function node(event: Input<"/input/topic">): Output {
  return {
    hello: "world!",
  };
};`,$e=L.ZP.div.withConfig({displayName:"UnsavedDot__src_panels_NodePlayground_index_tsx",componentId:"-8b6eme"})`
  display: ${({isSaved:n})=>n?"none":"initial"};
  width: 6px;
  height: 6px;
  border-radius: 50%;
  position: absolute;
  right: 8px;
  top: 50%;
  transform: translateY(-50%);
  background-color: ${D.O9.DARK9};
`,qe=L.ZP.div.withConfig({displayName:"SWelcomeScreen__src_panels_NodePlayground_index_tsx",componentId:"-163dlnr"})`
  display: flex;
  text-align: center;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 25%;
  height: 100%;
  > * {
    margin: 4px 0;
  }
`,_e=({addNewNode:n})=>{const{setHelpInfo:s}=(0,pe.w)(),{openHelp:l}=(0,Me.c)();return(0,e.jsxs)(qe,{children:[(0,e.jsx)(Je,{}),(0,e.jsxs)(ue.Z,{children:["Welcome to Node Playground! Get started by reading the"," ",(0,e.jsx)(se.r,{href:"",onClick:a=>{a.preventDefault(),s({title:"NodePlayground",content:q}),l()},children:"docs"}),", or just create a new node."]}),(0,e.jsxs)(W.Z,{style:{marginTop:"8px"},onClick:()=>n(),children:[(0,e.jsx)(z.Z,{size:"medium",children:(0,e.jsx)(Q.Z,{})})," ","New node"]})]})},et=Object.freeze({}),tt=n=>n.selectedLayout?.data?.userNodes??et;function nt(n){const{config:s,saveConfig:l}=n,{autoFormatOnSave:a=!1,selectedNodeId:o,editorForStorybook:i}=s,r=(0,T.F)(),[g,h]=f.useState(void 0),x=(0,U.NS)(tt),{state:{nodeStates:C,rosLib:v,typesLib:N}}=(0,H.BQ)(),{setUserNodes:j}=(0,U._B)(),B=(o!=null?C[o]?.diagnostics:void 0)??[],d=o!=null?x[o]:void 0,[c,I]=f.useState([]),y=c.length>0?c[c.length-1]:void 0,_=!!d&&!!y&&y.filePath===d.name,ee=!_||y.code===d.sourceCode,at=(o!=null?C[o]?.logs:void 0)??[],te=y?y.filePath+(y.readOnly?" (READONLY)":""):"node name",lt={borderRadius:0,margin:0,backgroundColor:r.semanticColors.bodyBackground,padding:"4px 20px",width:`${te.length+4}ch`};f.useLayoutEffect(()=>{if(d){const M=n.config.additionalBackStackItems??[];I([{filePath:d.name,code:d.sourceCode,readOnly:!1},...M])}},[n.config.additionalBackStackItems,d]);const V=f.useCallback(M=>{const m=(0,re.Z)(),w=M??Xe;j({[m]:{sourceCode:w,name:`${J.wd}${m.split("-")[0]}`}}),l({selectedNodeId:m})},[l,j]),ne=f.useCallback(M=>{o==null||M==null||M===""||!d||j({[o]:{...d,sourceCode:M}})},[d,o,j]),oe=f.useCallback((M,m)=>{m!=null&&m>0&&c.length>=m?I([...c.slice(0,m-1),M]):I([...c,M])},[c]),rt=f.useCallback(()=>{I(c.slice(0,c.length-1))},[c]),dt=f.useCallback(M=>{const m=[...c];if(m.length>0){const w=m.pop();w&&!w.readOnly&&I([...m,{...w,code:M}])}},[c]);return(0,e.jsxs)(A.Z,{height:"100%",children:[(0,e.jsx)(ce.Z,{floating:!0,helpContent:q}),(0,e.jsxs)(A.Z,{direction:"row",height:"100%",children:[(0,e.jsx)(He,{explorer:g,updateExplorer:h,selectNode:M=>{o!=null&&d&&y&&_&&j({[o]:{...d,sourceCode:y.code}}),l({selectedNodeId:M})},deleteNode:M=>{j({...x,[M]:void 0}),l({selectedNodeId:void 0})},selectedNodeId:o,userNodes:x,script:y,setScriptOverride:oe,addNewNode:V}),(0,e.jsxs)(A.Z,{flexGrow:1,height:"100%",overflow:"hidden",children:[(0,e.jsxs)(A.Z,{direction:"row",alignItems:"center",bgcolor:r.palette.neutralLighterAlt,children:[c.length>1&&(0,e.jsx)(z.Z,{size:"large",tooltip:"Go back",dataTest:"go-back",style:{color:D.O9.DARK9},onClick:rt,children:(0,e.jsx)(le.Z,{})}),o!=null&&d&&(0,e.jsxs)("div",{style:{position:"relative"},children:[(0,e.jsx)(F.tz,{type:"text",placeholder:"node name",value:te,disabled:!y||y.readOnly,style:lt,spellCheck:!1,onChange:M=>{const m=M.target.value;j({...x,[o]:{...d,name:m}})}}),(0,e.jsx)($e,{isSaved:ee})]}),(0,e.jsx)(z.Z,{size:"large",tooltip:"new node",dataTest:"new-node",style:{color:D.O9.DARK9,padding:"0 5px"},onClick:()=>V(),children:(0,e.jsx)(Q.Z,{})})]}),(0,e.jsxs)(A.Z,{flexGrow:1,overflow:"hidden ",children:[o==null&&(0,e.jsx)(_e,{addNewNode:V}),(0,e.jsx)(G.Z,{style:{flexGrow:1,width:"100%",overflow:"hidden",display:o!=null?"initial":"none"},children:(0,e.jsx)(p.Suspense,{fallback:(0,e.jsx)(A.Z,{direction:"row",flex:"auto",alignItems:"center",justifyContent:"center",width:"100%",height:"100%",children:(0,e.jsx)(ie.$,{size:ae.E.large})}),children:i??(0,e.jsx)(Ke,{autoFormatOnSave:a,script:y,setScriptCode:dt,setScriptOverride:oe,rosLib:v,typesLib:N,save:ne})})}),(0,e.jsx)(A.Z,{children:(0,e.jsx)(ve,{nodeId:o,isSaved:ee,save:()=>ne(y?.code),diagnostics:B,logs:at})})]})]})]})]})}const ot=[{key:"autoFormatOnSave",type:"toggle",title:"Auto-format on save"}],st={selectedNodeId:void 0,autoFormatOnSave:!0},it=(0,de.Z)(Object.assign(nt,{panelType:"NodePlayground",defaultConfig:st,configSchema:ot}))},94748:u=>{u.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTZEaa/1AAAAHUlEQVQYV2PYvXu3JAi7uLiAMaYAjAGTQBPYLQkAa/0Zef3qRswAAAAASUVORK5CYII="},4768:u=>{u.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAAL0lEQVQoz2NgCD3x//9/BhBYBWdhgFVAiVW4JBFKGIa4AqD0//9D3pt4I4tAdAMAHTQ/j5Zom30AAAAASUVORK5CYII="},64372:u=>{u.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAQAAADZc7J/AAAAz0lEQVRIx2NgYGBY/R8I/vx5eelX3n82IJ9FxGf6tksvf/8FiTMQAcAGQMDvSwu09abffY8QYSAScNk45G198eX//yev73/4///701eh//kZSARckrNBRvz//+8+6ZohwCzjGNjdgQxkAg7B9WADeBjIBqtJCbhRA0YNoIkBSNmaPEMoNmA0FkYNoFKhapJ6FGyAH3nauaSmPfwI0v/3OukVi0CIZ+F25KrtYcx/CTIy0e+rC7R1Z4KMICVTQQ14feVXIbR695u14+Ir4gwAAD49E54wc1kWAAAAAElFTkSuQmCC"},6161:u=>{u.exports="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNTMiIGhlaWdodD0iMzYiIHZpZXdCb3g9IjAgMCA1MyAzNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwKSI+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNNDguMDM2NCA0LjAxMDQySDQuMDA3NzlMNC4wMDc3OSAzMi4wMjg2SDQ4LjAzNjRWNC4wMTA0MlpNNC4wMDc3OSAwLjAwNzgxMjVDMS43OTcyMSAwLjAwNzgxMjUgMC4wMDUxODc5OSAxLjc5OTg0IDAuMDA1MTg3OTkgNC4wMTA0MlYzMi4wMjg2QzAuMDA1MTg3OTkgMzQuMjM5MiAxLjc5NzIxIDM2LjAzMTIgNC4wMDc3OSAzNi4wMzEySDQ4LjAzNjRDNTAuMjQ3IDM2LjAzMTIgNTIuMDM5IDM0LjIzOTIgNTIuMDM5IDMyLjAyODZWNC4wMTA0MkM1Mi4wMzkgMS43OTk4NCA1MC4yNDcgMC4wMDc4MTI1IDQ4LjAzNjQgMC4wMDc4MTI1SDQuMDA3NzlaTTguMDEwNDIgOC4wMTMwMkgxMi4wMTNWMTIuMDE1Nkg4LjAxMDQyVjguMDEzMDJaTTIwLjAxODIgOC4wMTMwMkgxNi4wMTU2VjEyLjAxNTZIMjAuMDE4MlY4LjAxMzAyWk0yNC4wMjA4IDguMDEzMDJIMjguMDIzNFYxMi4wMTU2SDI0LjAyMDhWOC4wMTMwMlpNMzYuMDI4NiA4LjAxMzAySDMyLjAyNlYxMi4wMTU2SDM2LjAyODZWOC4wMTMwMlpNNDAuMDMxMiA4LjAxMzAySDQ0LjAzMzlWMTIuMDE1Nkg0MC4wMzEyVjguMDEzMDJaTTE2LjAxNTYgMTYuMDE4Mkg4LjAxMDQyVjIwLjAyMDhIMTYuMDE1NlYxNi4wMTgyWk0yMC4wMTgyIDE2LjAxODJIMjQuMDIwOFYyMC4wMjA4SDIwLjAxODJWMTYuMDE4MlpNMzIuMDI2IDE2LjAxODJIMjguMDIzNFYyMC4wMjA4SDMyLjAyNlYxNi4wMTgyWk00NC4wMzM5IDE2LjAxODJWMjAuMDIwOEgzNi4wMjg2VjE2LjAxODJINDQuMDMzOVpNMTIuMDEzIDI0LjAyMzRIOC4wMTA0MlYyOC4wMjZIMTIuMDEzVjI0LjAyMzRaTTE2LjAxNTYgMjQuMDIzNEgzNi4wMjg2VjI4LjAyNkgxNi4wMTU2VjI0LjAyMzRaTTQ0LjAzMzkgMjQuMDIzNEg0MC4wMzEyVjI4LjAyNkg0NC4wMzM5VjI0LjAyMzRaIiBmaWxsPSIjNDI0MjQyIi8+CjwvZz4KPGRlZnM+CjxjbGlwUGF0aCBpZD0iY2xpcDAiPgo8cmVjdCB3aWR0aD0iNTMiIGhlaWdodD0iMzYiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="},51096:u=>{u.exports="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNTMiIGhlaWdodD0iMzYiIHZpZXdCb3g9IjAgMCA1MyAzNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwKSI+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNNDguMDM2NCA0LjAxMDQySDQuMDA3NzlMNC4wMDc3OSAzMi4wMjg2SDQ4LjAzNjRWNC4wMTA0MlpNNC4wMDc3OSAwLjAwNzgxMjVDMS43OTcyMSAwLjAwNzgxMjUgMC4wMDUxODc5OSAxLjc5OTg0IDAuMDA1MTg3OTkgNC4wMTA0MlYzMi4wMjg2QzAuMDA1MTg3OTkgMzQuMjM5MiAxLjc5NzIxIDM2LjAzMTIgNC4wMDc3OSAzNi4wMzEySDQ4LjAzNjRDNTAuMjQ3IDM2LjAzMTIgNTIuMDM5IDM0LjIzOTIgNTIuMDM5IDMyLjAyODZWNC4wMTA0MkM1Mi4wMzkgMS43OTk4NCA1MC4yNDcgMC4wMDc4MTI1IDQ4LjAzNjQgMC4wMDc4MTI1SDQuMDA3NzlaTTguMDEwNDIgOC4wMTMwMkgxMi4wMTNWMTIuMDE1Nkg4LjAxMDQyVjguMDEzMDJaTTIwLjAxODIgOC4wMTMwMkgxNi4wMTU2VjEyLjAxNTZIMjAuMDE4MlY4LjAxMzAyWk0yNC4wMjA4IDguMDEzMDJIMjguMDIzNFYxMi4wMTU2SDI0LjAyMDhWOC4wMTMwMlpNMzYuMDI4NiA4LjAxMzAySDMyLjAyNlYxMi4wMTU2SDM2LjAyODZWOC4wMTMwMlpNNDAuMDMxMiA4LjAxMzAySDQ0LjAzMzlWMTIuMDE1Nkg0MC4wMzEyVjguMDEzMDJaTTE2LjAxNTYgMTYuMDE4Mkg4LjAxMDQyVjIwLjAyMDhIMTYuMDE1NlYxNi4wMTgyWk0yMC4wMTgyIDE2LjAxODJIMjQuMDIwOFYyMC4wMjA4SDIwLjAxODJWMTYuMDE4MlpNMzIuMDI2IDE2LjAxODJIMjguMDIzNFYyMC4wMjA4SDMyLjAyNlYxNi4wMTgyWk00NC4wMzM5IDE2LjAxODJWMjAuMDIwOEgzNi4wMjg2VjE2LjAxODJINDQuMDMzOVpNMTIuMDEzIDI0LjAyMzRIOC4wMTA0MlYyOC4wMjZIMTIuMDEzVjI0LjAyMzRaTTE2LjAxNTYgMjQuMDIzNEgzNi4wMjg2VjI4LjAyNkgxNi4wMTU2VjI0LjAyMzRaTTQ0LjAzMzkgMjQuMDIzNEg0MC4wMzEyVjI4LjAyNkg0NC4wMzM5VjI0LjAyMzRaIiBmaWxsPSIjQzVDNUM1Ii8+CjwvZz4KPGRlZnM+CjxjbGlwUGF0aCBpZD0iY2xpcDAiPgo8cmVjdCB3aWR0aD0iNTMiIGhlaWdodD0iMzYiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="},62728:u=>{u.exports=`// This example shows how to publish a foxglove.LocationFix message
//
// https://foxglove.dev/docs/studio/messages/location-fix
//
// You can visualize this message with the Map panel
// https://foxglove.dev/docs/studio/panels/map

import { Input, Message } from "./types";

export const inputs = ["/input/topic"];
export const output = "/studio_node/my_gps";

// Our node will output a LocationFix message
type LocationFix = Message<"foxglove.LocationFix">;

export default function node(event: Input<"/input/topic">): LocationFix {
  return {
    latitude: 51.477928,
    longitude: -0.001545,
    altitude: 0,
    position_covariance_type: 0,
    position_covariance: new Float64Array(),
  };
};
`},79156:u=>{u.exports=`// This example shows how to publish a Marker message from a Node Playground node.
//
// Publishing Marker messages with a Node Playground script is a good way to visualize non-visual
// data.
//
// For example, if your robot calculates some projected paths and publishes them between two
// subsystems as a message, you can make a node that visualizes the path as a line list marker and view it in the 3D
// panel.

import { Input, Message } from "./types";

// The \`./markers\` utility provides a helper function to build a Marker.
import { buildRosMarker, MarkerTypes } from "./markers";

type GlobalVariables = { id: number };

export const inputs = ["/input/topic"];
export const output = "/studio_node/my_custom_topic";

// Our node will output a Marker message.
type Marker = Message<"visualization_msgs/Marker">;

// If you want to output multiple markers for a single input message, use a MarkerArray.
// The marker array message has one field, \`markers\`, which is an array of Marker messaages.
// type MarkerArray = Message<"visualization_msgs/MarkerArray">;

export default function node(event: Input<"/input/topic">, globalVars: GlobalVariables): Marker {
  return buildRosMarker({
      // Add any fields you want to set in the marker here
      // Any fields you omit will use default values
      // e.g 'type: MarkerTypes.ARROW' */
  });
};
`},62434:u=>{u.exports=`// This example shows how to subscribe to multiple input topics.
//
// NOTE:
// Node Playground scripts can subscribe to multiple input topics, but can only publish on a single topic.

import { Input } from "./types";

type Output = { topic: string };
type GlobalVariables = { id: number };

// List all the input topics in the \`input\` array
export const inputs = ["/input/topic", "/input/another"];
export const output = "/studio_node/output_topic";

// Make an InputEvent type alias. Since our node will get a message from either input topic, we need to enumerate the topics.
type InputEvent = Input<"/input/topic"> | Input<"/input/another">;

export default function node(event: InputEvent, globalVars: GlobalVariables): Output {
  // Remember that your node will get messages on each topic, so you
  // need to check each event's topic to know which fields are available on the message.
  switch (event.topic) {
    case "/input/topic":
      // topic specific input logic
      // Our message fields are specific to our topic message
      break;
    case "/input/another":
      // another specific logic
      break;
  }

  // Nodes can only output one type of message regardless of the inputs
  // Here we echo back the input topic as an example.
  return {
    topic: event.topic,
  };
};
`},32470:u=>{u.exports=`import { Input, Message } from "./types";

type Output = {};

export const inputs = ["/topic"];
export const output = "/studio_node/output_topic";

export default function node(event: Input<"/topic">): Output {
  return {};
};
`}}]);

//# sourceMappingURL=6008.7201ca5697b5c0e6ea70.js.map