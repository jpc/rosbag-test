"use strict";(self.webpackChunk=self.webpackChunk||[]).push([[6827],{76827:(e,d,n)=>{n.d(d,{v:()=>rn});var c=n(16259),m=n(45899),f=n(92231),u=n(66924),p=n(49173),h=n(17905);const b=[{fileName:"pointClouds.ts",sourceCode:m},{fileName:"readers.ts",sourceCode:f},{fileName:"time.ts",sourceCode:u},{fileName:"types.ts",sourceCode:p},{fileName:"vectors.ts",sourceCode:h},{fileName:"markers.ts",sourceCode:c}];var s=n(23621),y=n(11701),g=n(81468),v=n(76878),x=n(96627),w=n(5812),T=n(73406),R=n(90748),A=n(62227),P=n(4211),z=n(12771),M=n(16691),U=n(25385),E=n(1902),I=n(40453),C=n(63799),B=n(76100),D=n(36860),F=n(36412),N=n(41831),S=n(24855),j=n(12555),L=n(9080),G=n(19463),O=n(22656),V=n(42192),H=n(34518),Y=n(72931),k=n(88823),$=n(4092),Q=n(57495),W=n(31582),X=n(34607),J=n(66848),K=n(67780),Z=n(95681),_=n(39977),q=n(83597),nn=n(52822);const r="lib.d.ts",en=`
  declare type BaseTypes<T> = number | string | boolean | void | null | T;
  declare type LogArgs = { [key: string]: BaseTypes<LogArgs> | BaseTypes<LogArgs>[] };
  declare var log: (...args: Array<BaseTypes<LogArgs> | BaseTypes<LogArgs>[]>) => void;
`,o=new Map(Object.entries({es5:nn,es2015:x,es2016:E,es2017:I,es2018:L,es2019:Y,"es2015.core":v,"es2015.collection":g,"es2015.iterable":T,"es2015.generator":w,"es2015.promise":R,"es2015.proxy":A,"es2015.reflect":P,"es2015.symbol":z,"es2015.symbol.wellknown":M,"es2016.array.include":U,"es2017.object":B,"es2017.sharedmemory":D,"es2017.string":F,"es2017.intl":C,"es2017.typedarrays":N,"es2018.asynciterable":j,"es2018.asyncgenerator":S,"es2018.promise":O,"es2018.regexp":V,"es2018.intl":G,"es2019.array":H,"es2019.object":k,"es2019.string":$,"es2019.symbol":Q,"es2020.bigint":W,"es2020.promise":K,"es2020.sharedmemory":Z,"es2020.string":_,"es2020.symbol.wellknown":q,"es2020.intl":J}));function a(t){return t.replace(/\/\/\/ <reference lib="(.+)" \/>/g,(an,i)=>{const l=o.get(i);return o.delete(i),l==null?"":a(l)})}const tn=`${a(X)}

${en}`,sn=b.map(t=>({...t,filePath:`${y.wd}${t.fileName}`}));function rn(){const t=[];return t.push({fileName:r,filePath:r,sourceCode:tn}),{defaultLibFileName:r,rosLib:{fileName:s.Y,filePath:`/node_modules/${s.Y}`,sourceCode:s.k},declarations:t,utilityFiles:sn}}},16259:e=>{e.exports=`import { Header, Pose, Point, RGBA, Time } from "./types";

export interface IMarker {
  header: Header;
  ns: string;
  id: number;
  type: number;
  action: number;
  pose: Pose;
  scale: Point;
  color: RGBA;
  lifetime: Time;
  frame_locked: boolean;
  points: Point[];
  colors: RGBA[];
  text: string;
  mesh_resource: string;
  mesh_use_embedded_materials: boolean;
}

export type IRosMarker = IMarker;

/**
 * buildRosMarker builds a complete Marker message from an optional set of args.
 *
 * See https://foxglove.dev/docs/panels/3d for a list of supported Marker types
 *
 * @param args override any defaults in the marker fields
 * @returns an IRosMarker instance with default values for any omitted args
 */
export function buildRosMarker(args?: Partial<IRosMarker>): IRosMarker {
  const {
    header,
    ns,
    id,
    type,
    action,
    pose,
    scale,
    color,
    lifetime,
    frame_locked,
    points,
    colors,
    text,
    mesh_resource,
    mesh_use_embedded_materials,
  } = args ?? {};

  return {
    header: header ?? {
      frame_id: "",
      stamp: {
        sec: 0,
        nsec: 0,
      },
      seq: 0,
    },
    ns: ns ?? "",
    id: id ?? 0,
    type: type ?? 0,
    action: action ?? 0,
    pose: pose ?? {
      position: {
        x: 0,
        y: 0,
        z: 0,
      },
      orientation: {
        x: 0,
        y: 0,
        z: 0,
        w: 0,
      },
    },
    scale: scale ?? { x: 0, y: 0, z: 0 },
    color: color ?? { r: 0, g: 0, b: 0, a: 0 },
    lifetime: lifetime ?? { sec: 0, nsec: 0 },
    frame_locked: frame_locked ?? false,
    points: points ?? [],
    colors: colors ?? [],
    text: text ?? "",
    mesh_resource: mesh_resource ?? "",
    mesh_use_embedded_materials: mesh_use_embedded_materials ?? false,
  };
}

/**
 * Use this class to instantiate marker-like objects with defaulted values.
 *
 * @deprecated prefer \`buildRosMarker({ ... })\` instead
 */
export class Marker implements IMarker {
  header: Header = {
    frame_id: "",
    stamp: {
      sec: 0,
      nsec: 0,
    },
    seq: 0,
  };
  ns = "";
  id = 0;
  type = 0;
  action = 0;
  pose: Pose = {
    position: {
      x: 0,
      y: 0,
      z: 0,
    },
    orientation: {
      x: 0,
      y: 0,
      z: 0,
      w: 0,
    },
  };
  scale: Point = {
    x: 0,
    y: 0,
    z: 0,
  };
  color: RGBA = { r: 0, g: 0, b: 0, a: 0 };
  lifetime: Time = { sec: 0, nsec: 0 };
  frame_locked = false;
  points: Point[] = [];
  colors: RGBA[] = [];
  text = "";
  mesh_resource = "";
  mesh_use_embedded_materials = false;

  constructor({
    header,
    ns,
    id,
    type,
    action,
    pose,
    scale,
    color,
    lifetime,
    frame_locked,
    points,
    colors,
    text,
    mesh_resource,
    mesh_use_embedded_materials,
  }: Partial<IMarker>) {
    this.header = header ?? {
      frame_id: "",
      stamp: {
        sec: 0,
        nsec: 0,
      },
      seq: 0,
    };
    this.ns = ns ?? "";
    this.id = id ?? 0;
    this.type = type ?? 0;
    this.action = action ?? 0;
    this.pose = pose ?? {
      position: {
        x: 0,
        y: 0,
        z: 0,
      },
      orientation: {
        x: 0,
        y: 0,
        z: 0,
        w: 0,
      },
    };
    this.scale = scale ?? { x: 0, y: 0, z: 0 };
    this.color = color ?? { r: 0, g: 0, b: 0, a: 0 };
    this.lifetime = lifetime ?? { sec: 0, nsec: 0 };
    this.frame_locked = frame_locked ?? false;
    this.points = points ?? [];
    this.colors = colors ?? [];
    this.text = text ?? "";
    this.mesh_resource = mesh_resource ?? "";
    this.mesh_use_embedded_materials = mesh_use_embedded_materials ?? false;
  }
}
/**
 * Corresponds to the 'type' field of a marker.
 */
export enum MarkerTypes {
  ARROW = 0,
  CUBE = 1,
  SPHERE = 2,
  CYLINDER = 3,
  LINE_STRIP = 4,
  LINE_LIST = 5,
  CUBE_LIST = 6,
  SPHERE_LIST = 7,
  POINTS = 8,
  TEXT = 9,
  MESH = 10,
  TRIANGLE_LIST = 11,
}
`},45899:e=>{e.exports=`import { FieldReader, getReader } from "./readers";
import { Point, Header, RGBA } from "./types";

interface sensor_msgs__PointField {
  name: string;
  offset: number;
  datatype: number;
  count: number;
}

export interface sensor_msgs__PointCloud2 {
  header: Header;
  height: number;
  width: number;
  fields: sensor_msgs__PointField[];
  is_bigendian: boolean;
  point_step: number;
  row_step: number;
  data: Uint8Array;
  is_dense: boolean;
}

type Reader = { datatype: number; offset: number; reader: FieldReader };

function getFieldOffsetsAndReaders(fields: sensor_msgs__PointField[]): Reader[] {
  const result: Reader[] = [];
  for (const { datatype, offset = 0 } of fields) {
    result.push({ datatype, offset, reader: getReader(datatype, offset) });
  }
  return result;
}

type Field = number | string;

/**
 * Read points from a sensor_msgs.PointCloud2 message. Returns a nested array
 * of values whose index corresponds to that of the 'fields' value.
 */
export const readPoints = (message: sensor_msgs__PointCloud2): Array<Field[]> => {
  const { fields, height, point_step, row_step, width, data } = message;
  const readers = getFieldOffsetsAndReaders(fields);

  const points: Array<Field[]> = [];
  for (let i = 0; i < height; i++) {
    const dataOffset = i * row_step;
    for (let j = 0; j < width; j++) {
      const row: Field[] = [];
      const dataStart = j * point_step + dataOffset;
      for (const reader of readers) {
        const value = reader.reader.read(data, dataStart);
        row.push(value);
      }
      points.push(row);
    }
  }
  return points;
};

export function norm({ x, y, z }: Point): number {
  return Math.sqrt(x * x + y * y + z * z);
}

export function setRayDistance(pt: Point, distance: number): Point {
  const { x, y, z } = pt;
  const scale = distance / norm(pt);
  return {
    x: x * scale,
    y: y * scale,
    z: z * scale,
  };
}

// eslint-disable-next-line @foxglove/no-boolean-parameters
export function convertToRangeView(points: Point[], range: number, makeColors: boolean): RGBA[] {
  const colors: RGBA[] = makeColors ? new Array(points.length) : [];
  // First pass to get min and max ranges
  // TODO: Could be more efficient and extract this during
  // transforms for free
  let maxRange = Number.MIN_VALUE;
  if (makeColors) {
    for (const point of points) {
      maxRange = Math.max(maxRange, norm(point));
    }
  }
  // actually move the points and generate colors if specified
  for (let i = 0; i < points.length; ++i) {
    const pt = points[i]!;
    if (makeColors) {
      const dist = norm(pt);
      if (dist <= range) {
        // don't go all the way to white
        const extent = 0.8;
        // closest to target range is lightest,
        // closest to AV is darkest
        const other = (extent * dist) / range;
        colors[i] = { r: 1, g: other, b: other, a: 1 };
      } else {
        // don't go all the way to white
        const extent = 0.8;
        // closest to target range is lightest,
        // closest to max range is darkest
        const upper = maxRange - range;
        const other = extent * (1.0 - dist / upper);
        colors[i] = { r: other, g: other, b: 1, a: 1 };
      }
    }
    points[i] = setRayDistance(pt, range);
  }
  return colors;
}
`},92231:e=>{e.exports=`export const DATATYPE = {
  uint8: 2,
  uint16: 4,
  int16: 3,
  int32: 5,
  float32: 7,
};

export interface FieldReader {
  read(data: Uint8Array, index: number): number;
}

export class Float32Reader implements FieldReader {
  offset: number;
  view: DataView;
  constructor(offset: number) {
    this.offset = offset;
    const buffer = new ArrayBuffer(4);
    this.view = new DataView(buffer);
  }

  read(data: Uint8Array, index: number): number {
    const base = index + this.offset;
    const size = 4;
    if (data.length < base + size) {
      throw new Error("cannot read Float32 from data - not enough data");
    }
    this.view.setUint8(0, data[base]!);
    this.view.setUint8(1, data[base + 1]!);
    this.view.setUint8(2, data[base + 2]!);
    this.view.setUint8(3, data[base + 3]!);
    return this.view.getFloat32(0, true);
  }
}

export class Int32Reader implements FieldReader {
  offset: number;
  view: DataView;
  constructor(offset: number) {
    this.offset = offset;
    const buffer = new ArrayBuffer(4);
    this.view = new DataView(buffer);
  }

  read(data: Uint8Array, index: number): number {
    const base = index + this.offset;
    const size = 4;
    if (data.length < base + size) {
      throw new Error("cannot read Int32 from data - not enough data");
    }
    this.view.setUint8(0, data[base]!);
    this.view.setUint8(1, data[base + 1]!);
    this.view.setUint8(2, data[base + 2]!);
    this.view.setUint8(3, data[base + 3]!);
    return this.view.getInt32(0, true);
  }
}

export class Uint16Reader implements FieldReader {
  offset: number;
  view: DataView;
  constructor(offset: number) {
    this.offset = offset;
    const buffer = new ArrayBuffer(2);
    this.view = new DataView(buffer);
  }

  read(data: Uint8Array, index: number): number {
    const base = index + this.offset;
    const size = 2;
    if (data.length < base + size) {
      throw new Error("cannot read Uint16 from data - not enough data");
    }
    this.view.setUint8(0, data[base]!);
    this.view.setUint8(1, data[base + 1]!);
    return this.view.getUint16(0, true);
  }
}

export class Uint8Reader implements FieldReader {
  offset: number;
  constructor(offset: number) {
    this.offset = offset;
  }

  read(data: Uint8Array, index: number): number {
    const base = index + this.offset;
    const size = 1;
    if (data.length < base + size) {
      throw new Error("cannot read Uint8 from data - not enough data");
    }
    return data[base]!;
  }
}

export class Int16Reader implements FieldReader {
  offset: number;
  view: DataView;
  constructor(offset: number) {
    this.offset = offset;
    const buffer = new ArrayBuffer(2);
    this.view = new DataView(buffer);
  }

  read(data: Uint8Array, index: number): number {
    const base = index + this.offset;
    const size = 2;
    if (data.length < base + size) {
      throw new Error("cannot read Int16 from data - not enough data");
    }
    this.view.setUint8(0, data[base]!);
    this.view.setUint8(1, data[base + 1]!);
    return this.view.getInt16(0, true);
  }
}

export function getReader(datatype: number, offset: number): FieldReader {
  switch (datatype) {
    case DATATYPE.float32:
      return new Float32Reader(offset);
    case DATATYPE.uint8:
      return new Uint8Reader(offset);
    case DATATYPE.uint16:
      return new Uint16Reader(offset);
    case DATATYPE.int16:
      return new Int16Reader(offset);
    case DATATYPE.int32:
      return new Int32Reader(offset);
    default:
      throw new Error(\`Unsupported datatype: '\${datatype}'\`);
  }
}
`},66924:e=>{e.exports=`import { Time } from "./types";

/*
 * Checks ROS-time equality.
 */
export const areSame = (t1: Time, t2: Time): boolean => t1.sec === t2.sec && t1.nsec === t2.nsec;

/*
 * Compare two times, returning a negative value if the right is greater or a
 * positive value if the left is greater or 0 if the times are equal useful to
 * supply to Array.prototype.sort
 */
export const compare = (left: Time, right: Time): number => {
  const secDiff = left.sec - right.sec;
  return secDiff !== 0 ? secDiff : left.nsec - right.nsec;
};

const fixTime = (t: Time): Time => {
  // Equivalent to fromNanoSec(toNanoSec(t)), but no chance of precision loss.
  // nsec should be non-negative, and less than 1e9.
  let { sec, nsec } = t;
  while (nsec > 1e9) {
    nsec -= 1e9;
    sec += 1;
  }
  while (nsec < 0) {
    nsec += 1e9;
    sec -= 1;
  }
  return { sec, nsec };
};

export const subtractTimes = (
  { sec: sec1, nsec: nsec1 }: Time,
  { sec: sec2, nsec: nsec2 }: Time,
): Time => {
  return fixTime({ sec: sec1 - sec2, nsec: nsec1 - nsec2 });
};
`},49173:e=>{e.exports=`import { MessageTypeByTopic, MessageTypeBySchemaName } from "./generatedTypes";

/**
 * Message is a generic type for getting the type of a message for a schema name.
 *
 * \`\`\`
 * type GeometryPose = Message<"geometry_msgs/Pose">;
 * type PkgGeometryPose = Message<"pkg.geometry.Pose">;
 * \`\`\`
 */
export type Message<T extends keyof MessageTypeBySchemaName> = MessageTypeBySchemaName[T];

/**
 * Input type is a generic type for getting the event type on a topic.
 *
 * Most commonly used to type the input argument to your process function.
 *
 * \`\`\`
 * function process(msgEvent: Input<"/points">) { ... }
 * \`\`\`
 */
export type Input<T extends keyof MessageTypeByTopic> = {
  topic: T;
  receiveTime: Time;
  message: MessageTypeByTopic[T];
};

export type RGBA = {
  // all values are scaled between 0-1 instead of 0-255
  r: number;
  g: number;
  b: number;
  a: number; // opacity -- typically you should set this to 1.
};

export type Header = {
  frame_id: string;
  stamp: Time;
  seq: number;
};

export type Point = {
  x: number;
  y: number;
  z: number;
};

export type Time = {
  sec: number;
  nsec: number;
};

export type Translation = {
  x: number;
  y: number;
  z: number;
};

export type Rotation = {
  x: number;
  y: number;
  z: number;
  w: number;
};

export type Pose = {
  position: Point;
  orientation: Quaternion;
};

export type Quaternion = {
  x: number;
  y: number;
  z: number;
  w: number;
};

export type Transform = {
  header: Header;
  child_frame_id: string;
  transform: {
    translation: Translation;
    rotation: Rotation;
  };
};
`},17905:e=>{e.exports=`import { Point, Rotation } from "./types";

type vec3 = [number, number, number];

/*
 * Dot-product of two vectors.
 */
export function dot(vec1: number[], vec2: number[]): number {
  let ret = 0.0;
  for (let i = 0; i < vec1.length && i < vec2.length; ++i) {
    ret += vec1[i]! * vec2[i]!;
  }
  return ret;
}

/*
 * Cross-product of two vectors.
 */
export function cross(vec1: vec3, vec2: vec3): vec3 {
  const [ax, ay, az] = vec1;
  const [bx, by, bz] = vec2;
  return [ay * bz - az * by, az * bx - ax * bz, ax * by - ay * bx];
}

/*
 * Performs a rotation transformation on a point.
 */
export function rotate(rotation: Rotation, point: Point): Point {
  const v: vec3 = [point.x, point.y, point.z];

  // Extract the vector part of the quaternion
  const u: vec3 = [rotation.x, rotation.y, rotation.z];

  // Extract the scalar part of the quaternion
  const s = -1 * rotation.w;

  // Do the math
  const t1 = scalarMultiply(u, 2.0 * dot(u, v));
  const t2 = scalarMultiply(v, s * s - dot(u, u));
  const t3 = scalarMultiply(cross(u, v), 2 * s);
  const d = vectorAddition([t1, t2, t3]);

  return {
    x: d[0]!,
    y: d[1]!,
    z: d[2]!,
  };
}

/*
 * Scales a vector.
 */
export function scalarMultiply(vector: number[], scalar: number): number[] {
  const ret = vector.slice();
  let i;
  for (i = 0; i < ret.length; ++i) {
    ret[i] *= scalar;
  }
  return ret;
}

/*
 * Sums an array of vectors.
 * NOTE: all the vector arrays must be at least the length of the first vector
 */
export function vectorAddition(vectors: number[][]): number[] {
  const first = vectors[0];
  if (!first) {
    throw new Error("vectorAddition requires vectors");
  }

  const ret = first.slice();
  for (let i = 1; i < vectors.length; ++i) {
    for (let j = 0; j < ret.length; ++j) {
      ret[j] += vectors[i]![j]!;
    }
  }
  return ret;
}
`}}]);

//# sourceMappingURL=6827.1c098233f9e70591024d.js.map