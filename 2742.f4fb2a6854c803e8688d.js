"use strict";(self.webpackChunk=self.webpackChunk||[]).push([[2742],{22742:(F,P,t)=>{t.r(P),t.d(P,{ANIMATION_RESET_DELAY_MS:()=>p,default:()=>G});var e=t(52322),A=t(76635),s=t(2784),C=t(60669),S=t(29268),f=t(66690),T=t(37825),u=t(1831),j=t(92168),x=t(88058),R=t(76294),g=t(24304),l=t(22915),v=t(77075);const E=(0,f.Le)(l.iv`
    background: transparent;
  `,l.iv`
    background: ${v.O9.HIGHLIGHT_MUTED};
  `),I=3,M=l.ZP.tr.withConfig({displayName:"AnimatedRow__src_panels_Parameters_AnimatedRow_tsx",componentId:"-176oz7u"})`
  background: transparent;
  animation: ${({animate:a,skipAnimation:i})=>a&&!i?E:"none"}
    ${I}s ease-in-out;
  animation-iteration-count: 1;
  animation-fill-mode: forwards;
  border-bottom: 1px solid ${v.O9.BORDER_LIGHT};
`,N=l.ZP.div.withConfig({displayName:"ParametersPanel__src_panels_Parameters_ParametersPanel_tsx",componentId:"-1k51ukg"})`
  display: flex;
  flex-direction: column;
`,w=l.ZP.div.withConfig({displayName:"ParametersTable__src_panels_Parameters_ParametersTable_tsx",componentId:"-z9i7v7"})`
  display: flex;
  flex-direction: column;
  white-space: nowrap;
  color: ${({theme:a})=>a.palette.neutralPrimary};

  table {
    width: calc(100% + 1px);
  }

  thead {
    user-select: none;
    border-bottom: 1px solid ${({theme:a})=>a.semanticColors.bodyDivider};
  }

  th,
  td {
    padding: 3px 16px;
    line-height: 100%;
    border: none;
  }

  tr:first-child th {
    padding: 8px 16px;
    border: none;
    text-align: left;
    color: ${({theme:a})=>a.palette.neutralSecondary};
    min-width: 120px;
  }

  td {
    input {
      background: none !important;
      color: inherit;
      width: 100%;
      padding-left: 0;
      padding-right: 0;
      min-width: 40px;
    }
    &:last-child {
      color: rgba(255, 255, 255, 0.6);
    }
  }
`,Z=l.ZP.div.withConfig({displayName:"Scrollable__src_panels_Parameters_Scrollable_tsx",componentId:"-141hcl2"})`
  height: 100%;
  width: 100%;
  overflow-y: auto;
  position: absolute;
`;var y=t(63878);const p=3e3,$=new Map;function D(a){return a.playerState.capabilities}function O(a){return a.setParameter}function L(a){return a.playerState.activeData?.parameters??$}function h(){const a=(0,u.An)(D),i=(0,u.An)(O),n=(0,u.An)(L),V=(0,C.Z)((0,s.useCallback)((r,o)=>{i(r,o)},[i]),200),H=a.includes(g.C.getParameters),Y=a.includes(g.C.setParameters),k=(0,s.useMemo)(()=>Array.from(n.keys()),[n]),c=(0,s.useRef)(!0);(0,s.useEffect)(()=>{const r=setTimeout(()=>c.current=!1,p);return()=>clearTimeout(r)},[]);const d=(0,s.useRef)(n),[z,b]=(0,s.useState)([]);return(0,s.useEffect)(()=>{if(c.current||(0,f.MC)()){d.current=n;return}const r=(0,A.union)(Array.from(n.keys()),Array.from(d.current?.keys()??[])).filter(m=>d.current?.get(m)!==n.get(m));b(r),d.current=n;const o=setTimeout(()=>b([]),p);return()=>clearTimeout(o)},[n,c]),H?(0,e.jsxs)(N,{children:[(0,e.jsx)(x.Z,{helpContent:y,floating:!0}),(0,e.jsx)(Z,{children:(0,e.jsx)(w,{children:(0,e.jsxs)(T.kp,{children:[(0,e.jsx)("thead",{children:(0,e.jsxs)("tr",{children:[(0,e.jsx)("th",{children:"Parameter"}),(0,e.jsx)("th",{children:"Value"})]})}),(0,e.jsx)("tbody",{children:k.map(r=>{const o=JSON.stringify(n.get(r))??"";return(0,e.jsxs)(M,{skipAnimation:c.current,animate:z.includes(r),children:[(0,e.jsx)("td",{children:r}),(0,e.jsx)("td",{width:"100%",children:Y?(0,e.jsx)(R.V,{dataTest:`parameter-value-input-${o}`,value:o,onChange:m=>{V(r,m)}}):o})]},`parameter-${r}`)})})]})})})]}):(0,e.jsxs)(e.Fragment,{children:[(0,e.jsx)(x.Z,{floating:!0,helpContent:y}),(0,e.jsx)(S.Z,{children:"Connect to a ROS source to view parameters"})]})}h.panelType="Parameters",h.defaultConfig={};const G=(0,j.Z)(h)}}]);

//# sourceMappingURL=2742.f4fb2a6854c803e8688d.js.map