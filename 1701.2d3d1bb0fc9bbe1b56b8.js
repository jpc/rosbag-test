"use strict";(self.webpackChunk=self.webpackChunk||[]).push([[1701],{21701:(kt,U,t)=>{t.r(U),t.d(U,{default:()=>Wt,openSiblingStateTransitionsPanel:()=>Ht,transitionableRosTypes:()=>J});var i=t(52322),nt=t(76635),d=t(2784),st=t(62515),B=t(22915),ot=t(98621),D=t.n(ot),W=t(45901),y=t(44668),Z=t(65111),at=t(43017),it=t(35870),z=t(1012),Y=t(36922);function rt(e,l=1/0){const s=(0,W.O4)(e),m=(0,d.useMemo)(()=>(0,z.UU)(s),[s]),c=Z.V7({topics:m,historySize:l}),S=(0,Y.wm)(s);return(0,d.useMemo)(()=>S(c),[S,c])}var V=t(1831),lt=t(92168),ct=t(88058),dt=t(75657),ht=t(36035),pt=t(8314),ut=t(66198),mt=t(72881),ft=t(80078),gt=t(27790),E=t(32736),vt=t(2784);function xt(e,l){const s=(0,E.eT)((0,E.pb)(l)[e.datatype],[{type:"name",name:"header",repr:"header"},{type:"name",name:"stamp",repr:"stamp"}]);return!s.valid||!(0,E.Ky)(s.structureItem,["time"])}const yt=(0,gt.Z)(e=>({iconButton:{"&.MuiIconButton-root":{color:e.palette.text.secondary,padding:e.spacing(.375),"&:hover":{color:e.palette.text.primary}}},checkIcon:{marginLeft:e.spacing(2)}}));function St(e){const l=yt(),[s,m]=vt.useState(void 0),c=Boolean(s),S=h=>{m(h.currentTarget)},{path:T,timestampMethod:f="receiveTime",iconButtonProps:C}=e,{datatypes:p,topics:v}=Z.Ui(),g=(0,d.useMemo)(()=>(0,z.ZP)(T),[T]),M=(0,d.useMemo)(()=>{if(!g)return;const{topicName:h}=g;return v.find(({name:I})=>I===h)},[g,v]),x=(0,d.useMemo)(()=>M?xt(M,p):!1,[p,M]),P=e.onTimestampMethodChange,b=(0,d.useCallback)(h=>{P?.(h,e.index)},[P,e.index]),o=[{label:"Receive time",value:"receiveTime"},{label:"header.stamp",value:"headerStamp"}];return(0,i.jsxs)(i.Fragment,{children:[(0,i.jsx)(ut.Z,{size:"small",id:"timestamp-method-button",className:l.iconButton,"aria-controls":c?"timestamp-method-menu":void 0,"aria-expanded":c?"true":void 0,"aria-haspopup":"true",onClick:S,...C,children:(0,i.jsx)(ht.Z,{fontSize:"inherit"})}),(0,i.jsx)(mt.Z,{id:"timestamp-method-menu",anchorEl:s,open:c,onClose:()=>m(void 0),MenuListProps:{"aria-labelledby":"timestamp-method-button",dense:!0,disablePadding:!0},children:o.map(h=>(0,i.jsxs)(ft.Z,{disabled:x&&h.value==="headerStamp",selected:f===h.value,onClick:()=>{b(h.value),m(void 0)},children:[h.label,f===h.value&&(0,i.jsx)(pt.Z,{className:l.checkIcon,fontSize:"small"})]},h.value))})]})}var Tt=t(73821),Ct=t(77075),Mt=t(7357),Pt=t(9839),bt=t.n(Pt),X=t(68071),It=t(27999),Bt=t(25302);function jt(e,l){return(e%l+l)%l}const k=[Bt.B,...X.Hm];function G(e){const{path:l,startTime:s,y:m,pathIndex:c,blocks:S}=e,T={datasets:[],tooltips:[]};let f,C,p=[];for(const v of S){if(!v){p=[],f=void 0,C=void 0;continue}for(const g of v){const M=(0,It.IJ)(g.messageEvent.message),x=l.timestampMethod==="headerStamp"?M:g.messageEvent.receiveTime;if(!x)continue;const P=g.queriedData[0];if(g.queriedData.length!==1||!P)continue;const{constantName:b,value:o}=P;if(C&&(0,y.toSec)((0,y.subtract)(C,x))===0&&f===o||(C=x,Number.isNaN(o)&&typeof o!="string")||typeof o!="number"&&typeof o!="bigint"&&typeof o!="boolean"&&typeof o!="string")continue;const h=typeof o=="string"?bt()(o):Math.round(Number(o)),I=k[jt(h,Object.values(k).length)]??"grey",R=(0,y.toSec)((0,y.subtract)(x,s)),N={x:R,y:m},$={x:R,y:m,path:l.value,value:o,constantName:b};if(T.tooltips.unshift($),p.push({x:R,y:m}),o!==f){const F=b!=null?`${b} (${String(o)})`:String(o);p=[{...N,label:F,labelColor:I}];const H={borderWidth:10,borderColor:I,data:p,label:c.toString(),pointBackgroundColor:(0,X.eE)(I),pointBorderColor:"transparent",pointHoverRadius:3,pointRadius:1.25,pointStyle:"circle",showLine:!0,datalabels:{color:I}};T.datasets.push(H)}f=o}}return T}var Rt=t(2784);const J=["bool","int8","uint8","int16","uint16","int32","uint32","int64","uint64","string","json"],Zt=Ct.Rq.MONOSPACE,Nt=10,At="bold",Dt=B.ZP.div.withConfig({displayName:"SRoot__src_panels_StateTransitions_index_tsx",componentId:"-tchwfu"})`
  display: flex;
  flex-grow: 1;
  z-index: 0; // create new stacking context
  overflow: hidden;
`,zt=B.ZP.div.withConfig({displayName:"SAddButton__src_panels_StateTransitions_index_tsx",componentId:"-1ofgowg"})`
  position: absolute;
  top: 30px;
  right: 5px;
  z-index: 1;
`,Et=B.ZP.div.withConfig({displayName:"SChartContainerOuter__src_panels_StateTransitions_index_tsx",componentId:"-qrls1p"})`
  width: 100%;
  flex-grow: 1;
  overflow-x: hidden;
  overflow-y: auto;
`,Lt=B.ZP.div.withConfig({displayName:"SChartContainerInner__src_panels_StateTransitions_index_tsx",componentId:"-en1lp2"})`
  position: relative;
  margin-top: 10px;
`,L=20,K=B.ZP.div.withConfig({displayName:"SInputContainer__src_panels_StateTransitions_index_tsx",componentId:"-n8050h"})`
  display: flex;
  position: absolute;
  padding-left: ${L}px;
  margin-top: -2px;
  height: 20px;
  padding-right: 4px;
  max-width: calc(100% - ${L}px);
  min-width: min(100%, 150px); // Don't let it get too small.
  overflow: hidden;
  line-height: 20px;

  &:hover {
    background: ${({theme:e})=>D()(e.palette.neutralLight).setAlpha(.5).toRgbString()};
  }

  // Move over the first input on hover for the toolbar.
  ${e=>e.shrink&&B.iv`
      max-width: calc(100% - 150px);
    `}
`,$t=B.ZP.div.withConfig({displayName:"SInputDelete__src_panels_StateTransitions_index_tsx",componentId:"-1mtg2mt"})`
  display: none;
  position: absolute;
  left: ${L}px;
  transform: translateX(-100%);
  user-select: none;
  height: 20px;
  line-height: 20px;
  padding: 0 6px;
  background: ${({theme:e})=>D()(e.palette.neutralLight).setAlpha(.5).toRgbString()};
  cursor: pointer;

  &:hover {
    background: ${({theme:e})=>D()(e.palette.neutralLight).setAlpha(.75).toRgbString()};
  }

  ${K}:hover & {
    display: block;
  }
`,Ft={datalabels:{display:"auto",anchor:"center",align:-45,offset:6,clip:!0,font:{family:Zt,size:Nt,weight:At}},zoom:{zoom:{enabled:!0,mode:"x",sensitivity:3,speed:.1},pan:{mode:"x",enabled:!0,speed:20,threshold:10}}};function Ht(e,l){e({panelType:"StateTransitions",updateIfExists:!0,siblingConfigCreator:s=>({...s,paths:(0,nt.uniq)(s.paths.concat([{value:l,timestampMethod:"receiveTime"}]))})})}function Ot(e){return e.playerState.activeData?.currentTime}const wt=Rt.memo(function(l){const{config:s,saveConfig:m}=l,{paths:c}=s,S=(a,r)=>{if(r==null)throw new Error("index not set");const n=s.paths.slice(),u=n[r];u&&(n[r]={...u,value:a.trim()}),m({paths:n})},T=(a,r)=>{if(r==null)throw new Error("index not set");const n=s.paths.slice(),u=n[r];u&&(n[r]={...u,timestampMethod:a}),m({paths:n})},f=(0,d.useMemo)(()=>c.map(({value:a})=>a),[c]),C=(0,d.useMemo)(()=>(0,z.UU)(f),[f]),{startTime:p}=Z.Ui(),v=(0,V.An)(Ot),g=(0,d.useMemo)(()=>!v||!p?void 0:(0,y.toSec)((0,y.subtract)(v,p)),[v,p]),M=rt(f),x=(0,Y.wm)(f),P=(0,Z.Bv)(C),b=(0,d.useMemo)(()=>P.map(x),[P,x]),{height:o,heightPerTopic:h}=(0,d.useMemo)(()=>{const a=c.length*55;return{height:Math.max(80,a+30),heightPerTopic:a/c.length}},[c.length]),{datasets:I,tooltips:R,minY:N}=(0,d.useMemo)(()=>{let a;const r=[],n=[];return p?(c.forEach((u,O)=>{const A=(O+1)*6*-1;a=Math.min(a??A,A-3);const tt=b.map(j=>j[u.value]);{const{datasets:j,tooltips:w}=G({path:u,startTime:p,y:A,pathIndex:O,blocks:tt});n.push(...j),r.push(...w)}if(tt.some(j=>j!=null))return;const et=M[u.value];if(et){const{datasets:j,tooltips:w}=G({path:u,startTime:p,y:A,pathIndex:O,blocks:[et]});n.push(...j),r.push(...w)}}),{datasets:n,tooltips:r,minY:a}):{datasets:n,tooltips:r,minY:a}},[M,b,c,p]),$=(0,d.useMemo)(()=>({ticks:{display:!1},type:"linear",min:N,max:-3}),[N]),F=(0,d.useMemo)(()=>({type:"linear"}),[]),{width:Q,ref:H}=(0,st.NB)({handleHeight:!1,refreshRate:0,refreshMode:"debounce"}),q=(0,V.m7)(),Yt=(0,d.useCallback)(({x:a})=>{const{seekPlayback:r,playerState:{activeData:{startTime:n}={}}}=q();if(!r||a==null||n==null)return;const u=(0,y.add)(n,(0,y.fromSec)(a));r(u)},[q]),Vt=(0,W.O4)({datasets:I}),_=(0,d.useRef)(null),Xt=(0,Tt.j)(_);return(0,i.jsxs)(Dt,{ref:_,children:[(0,i.jsx)(ct.Z,{floating:!0,helpContent:Mt}),(0,i.jsx)(zt,{style:{visibility:Xt?"visible":"hidden"},children:(0,i.jsx)(at.Z,{onClick:()=>m({paths:[...s.paths,{value:"",timestampMethod:"receiveTime"}]}),children:"add"})}),(0,i.jsx)(Et,{children:(0,i.jsxs)(Lt,{style:{height:o},ref:H,children:[(0,i.jsx)(dt.Z,{zoom:!0,isSynced:!0,showXAxisLabels:!0,width:Q??0,height:o,data:Vt,type:"scatter",xAxes:F,xAxisIsPlaybackTime:!0,yAxes:$,plugins:Ft,tooltips:R,onClick:Yt,currentTime:g}),c.map(({value:a,timestampMethod:r},n)=>(0,i.jsxs)(K,{style:{top:n*h},shrink:n===0,children:[(0,i.jsx)($t,{onClick:()=>{const u=s.paths.slice();u.splice(n,1),m({paths:u})},children:"\u2715"}),(0,i.jsx)(it.ZP,{path:a,onChange:S,index:n,autoSize:!0,validTypes:J,noMultiSlices:!0}),(0,i.jsx)(St,{path:a,index:n,iconButtonProps:{disabled:!a},timestampMethod:r,onTimestampMethodChange:T})]},n))]})})]})}),Ut={paths:[]},Wt=(0,lt.Z)(Object.assign(wt,{panelType:"StateTransitions",defaultConfig:Ut}))}}]);

//# sourceMappingURL=1701.2d3d1bb0fc9bbe1b56b8.js.map